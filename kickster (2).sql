-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 14, 2016 at 02:39 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kickster`
--

-- --------------------------------------------------------

--
-- Table structure for table `brand_tbl`
--

CREATE TABLE `brand_tbl` (
  `brand_id` int(11) NOT NULL,
  `brand_name` varchar(10) NOT NULL,
  `brand_logo` text NOT NULL,
  `brand_img_src` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `brand_tbl`
--

INSERT INTO `brand_tbl` (`brand_id`, `brand_name`, `brand_logo`, `brand_img_src`) VALUES
(0, 'Adidas', 'resources/images/brands/adidas_logo.jpg\r\nresources/images/brands/adidas_logo_white.png', 'resources/topic/adidas/adidas_brand_home/adidas_wallpaper_nmd_2.jpg'),
(1, 'Asics', 'resources/images/brands/asics_logo.jpg\r\nresources/images/brands/asics_logo_white.png', 'resources/topic/asics/asics_brand_home/wallpaper_asics.jpg'),
(2, 'Nike', 'resources/images/brands/nike_logo.png\r\nresources/images/brands/nike_logo_white.png', 'resources/topic/nike/nike_brand_home/wallpaper.jpg'),
(3, 'diadora', 'resources/images/brands/diadora_logo.jpg\r\nresources/images/brands/diadora_logo.jpg', 'resources/topic/diadora/wallpaper_diadora.jpg'),
(4, 'Saucony', 'resources/images/brands/saucony_logo.jpg\r\nresources/images/brands/saucony_logo_clear.png\r\n', 'resources/topic/saucony/saucony_wallpaper.jpg'),
(5, 'Reebok', 'resources/images/brands/reebok_logo.jpg\r\nresources/images/brands/reebok_logo.jpg\r\n', 'resources/topic/reebok/reebok_wallpaper.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `cart_detail`
--

CREATE TABLE `cart_detail` (
  `cart_detail_id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `size` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart_detail`
--

INSERT INTO `cart_detail` (`cart_detail_id`, `cart_id`, `prod_id`, `size`) VALUES
(11, 17, 41, '8'),
(12, 17, 3, '7'),
(13, 17, 1, '7');

-- --------------------------------------------------------

--
-- Table structure for table `cart_tbl`
--

CREATE TABLE `cart_tbl` (
  `cart_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cart_amount` double NOT NULL,
  `cart_quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cart_tbl`
--

INSERT INTO `cart_tbl` (`cart_id`, `user_id`, `cart_amount`, `cart_quantity`) VALUES
(17, 3, 27704, 3);

-- --------------------------------------------------------

--
-- Table structure for table `product_tbl`
--

CREATE TABLE `product_tbl` (
  `prod_id` int(10) UNSIGNED NOT NULL,
  `prod_name` varchar(50) NOT NULL,
  `prod_desc` text NOT NULL,
  `prod_size` varchar(50) NOT NULL,
  `prod_price` double NOT NULL,
  `prod_price_before` double DEFAULT NULL,
  `brand_id` char(10) NOT NULL,
  `prod_img_src` text NOT NULL,
  `rating` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_tbl`
--

INSERT INTO `product_tbl` (`prod_id`, `prod_name`, `prod_desc`, `prod_size`, `prod_price`, `prod_price_before`, `brand_id`, `prod_img_src`, `rating`) VALUES
(1, 'N9000 MM\r', 'The iconic N9000 revisited, using performance materials in the upper. The outsole faithfully reproduces the original construction, making the shoe comfortable.\r', '7,7.5,9.5,10,11,12\r', 7000, 7235.53, '3', 'resources/topic/diadora/diadora_N9000_Black/diadora_N9000_MM(1).jpg./.resources/topic/diadora/diadora_N9000_Black/diadora_N9000_MM(2).jpg./.resources/topic/diadora/diadora_N9000_Black/diadora_N9000_MM(3).jpg\r', 0),
(2, 'ASICS GEL-Lyte III "Black Mamba"\r', 'The ASICS GEL-Lyte III "Black Mamba" is a reptilian-inspired sneaker that offers a darker alternative to the loud, colorful sneakers being released this season. Showcased in a triple-black colorway, the sneaker sports scaly construction throughout with branding texturally juxtaposed in black suede and embossed on the midsole and heel counter. Minimal hints of white in the laces provide the only offset in this offering. A release date has not been confirmed but the ASICS GEL-Lyte III "Black Mamba" will retail for $130 USD and you can pick up a pair via Oneness’ online store this August.\r', '8,8.5,9.5,10,10.5,11,11.5\r', 5720, 0, '1', 'resources/topic/asics/asics_gel_lyte_iii_black_mamba/asics_gel_lyte_iii_black_mamba_0001.jpg./.resources/topic/asics/asics_gel_lyte_iii_black_mamba/asics_gel_lyte_iii_black_mamba_02.jpg./.resources/topic/asics/asics_gel_lyte_iii_black_mamba/asics_gel_lyte_iii_black_mamba_03.jpg\r', 9),
(3, 'Trident Brogue St Moritz\r', 'The Trident Brogue Saint Moritz features a luxurious suede upper with Oxford detailing, for a hand-crafted, elegant effect that is perfectly suited to any type of occasion.\r', '7,7.5,9.5,10,11,12\r', 9000, 9346.46, '3', 'resources/topic/diadora/Trident_Brogue_St_Moritz/Trident_Brogue_St_Moritz(1).jpg./.resources/topic/diadora/Trident_Brogue_St_Moritz/Trident_Brogue_St_Moritz(2).jpg./.resources/topic/diadora/Trident_Brogue_St_Moritz/Trident_Brogue_St_Moritz(3).jpg\r', 6),
(4, 'NIKE AIR MAX 1 ULTRA FLYKNIT\r', 'The Air Max 1 gets remixed with breathable Flyknit for the first time. A cored-out Ultra midsole and plush Air Max cushioning complete the sneaker, delivering featherweight, all-day comfort.\r', '7,7.5,9.5,10,11,12\r', 7653.68, 0, '2', 'resources/topic/nike/NIKE_AIR_MAX_1_ULTRA_FLYKNIT/NIKE_AIR_MAX_1_ULTRA_FLYKNIT(1).jpg./.resources/topic/nike/NIKE_AIR_MAX_1_ULTRA_FLYKNIT/NIKE_AIR_MAX_1_ULTRA_FLYKNIT(2).jpg./.resources/topic/nike/NIKE_AIR_MAX_1_ULTRA_FLYKNIT/NIKE_AIR_MAX_1_ULTRA_FLYKNIT(3).jpg\r', 10),
(5, 'Mi Basket Power\r', 'A Diadora icon developed in 1984 to celebrate the brand sponsorship of the Milan basketball team. This time around, the model is revisited with a high-fashion, luxury touch, in full grain leather with a gloss-effect wax finish, available in two stylish colours.\r', '7,7.5,9.5,10,11,12\r', 4849.74, 0, '3', 'resources/topic/diadora/Mi_Basker_Power/Mi_Basket_Power(1).jpg./.resources/topic/diadora/Mi_Basker_Power/Mi_Basket_Power(2).jpg./.resources/topic/diadora/Mi_Basker_Power/Mi_Basket_Power(3).jpg\r', 0),
(6, 'ZX Flux ADV X\r', 'The ZX family of running shoes caught the attention of high-mileage runners around 1984 with a futuristic look and feel. .These shoes are crafted in premium nubuck with a single-piece fold-over tongue construction. Welded trim and a neoprene heel cage give it a touch of technical style.\r', '4.5, 5, 6, 6.5, 7.5, 11\r', 6495, 0, '0', 'resources/topic/adidas/ZXfLuxADVX/S76364_BL_eCom.jpg./.resources/topic/adidas/ZXfLuxADVX/S76364_FM_eCom.jpg./.resources/topic/adidas/ZXfLuxADVX/S76364_SL_eCom.jpg\r', 3),
(7, 'B.elite Liquid\r', 'Stylish interpretation in quality full-grain leather of the Diadora model which has had the greatest success in the most famous tennis tournaments in the world. Model embellished with inner “starry" padding, which lends the shoe a creative touch.\r', '8,8.5,9.5,10,11,11.5\r', 6200, 6384.79, '3', 'resources/topic/diadora/B.elite_Liquid/B.elite_Liquid(1).jpg./.resources/topic/diadora/B.elite_Liquid/B.elite_Liquid(2).jpg./.resources/topic/diadora/B.elite_Liquid/B.elite_Liquid(3).jpg\r', 6),
(8, 'Exodus Nyl\r', 'The Exodus Nyl model is inspired by the 90s and has a nylon upper with suede trims. The special vintage waxing lends this shoe a sporty and vintage feel at the same time.\r', '8,8.5,9.5,10,11,11.5\r', 8500, 9073.72, '3', 'resources/topic/diadora/Exodus_Nyl/Exodus_Nyl(1).jpg./.resources/topic/diadora/Exodus_Nyl/Exodus_Nyl(2).jpg./.resources/topic/diadora/Exodus_Nyl/Exodus_Nyl(3).jpg\r', 2),
(9, 'Ultraboost\r', 'ULTRA BOOST SHOES COMFORTABLE ENERGY-RETURNING SHOES MADE FOR NATURAL RUNNING, RAIN OR SHINE.\r', '8,8.5,9.5,10,11,11.5\r', 7000, 0, '0', 'resources/topic/adidas/ultraboost/front_up_ultraboost.jpg./.resources/topic/adidas/ultraboost/sideface.jpg./.resources/topic/adidas/ultraboost/soleface.jpg\r', 9),
(10, 'Crazy Explosive\r', 'They pair a textile and synthetic upper with a full-length boost™ midsole that offers energy return in every step.Designed to work with the muscles, bones and tendons of the foot, the unique lace system provides an enhanced fit.\r', '9, 9.5\r', 7995, 0, '0', 'resources/topic/adidas/CrazyExplosive/B42424_BL_eCom.jpg./.resources/topic/adidas/CrazyExplosive/B42424_FM_eCom.jpg./.resources/topic/adidas/CrazyExplosive/B42424_SL_eCom.jpg\r', 1),
(11, 'Crazy Explosive Primeknit\r', 'Explode past the defence in these men multicoloured basketball shoes. With a flexible adidas Primeknit upper, they feature a full-length boost™ midsole for ultimate energy return and a unique lacing system for an enhanced fit.\r', '7.5, 8, 8.5, 9, 9.5, 10, 10.5\r', 8795, 0, '0', 'resources/topic/adidas/CrazyExplosivePrimeKnit/AQ7218_BL_eCom.jpg./.resources/topic/adidas/CrazyExplosivePrimeKnit/AQ7218_FM_eCom.jpg./.resources/topic/adidas/CrazyExplosivePrimeKnit/AQ7218_LSL_eCom.jpg\r', 8),
(12, 'D Lillard 2.0\r', 'Rising out of Oakland, Damian Lillard emerged as a star player with a tireless work ethic. Designed after his style of play, these men basketball shoes are cut low for ankle mobility with extra support in the forefoot, great for stepback jump shots.\r', '7, 7.5, 8, 8.5, 9, 9.5, 10, 10.5, 11, 11.5, 12, 13', 5995, 0, '0', 'resources/topic/adidas/DLillard2.0/B42386_BL_eCom.jpg./.resources/topic/adidas/DLillard2.0/B42386_FM_eCom.jpg./.resources/topic/adidas/DLillard2.0/B42386_SL_eCom.jpg\r', 2),
(13, 'Crazy Light Boost Low 2016\r', 'Capitalise on the fast break in these men basketball shoes. Designed to burst past the defence, they ride a full-length boost™ midsole for energy return with every step.\r', '7.5, 8.5, 9, 9.5, 10, 10.5, 11, 11.5, 12, 13.5\r', 7995, 0, '0', 'resources/topic/adidas/CrazylightBoostLow2016/B54172_BL_eCom.jpg./.resources/topic/adidas/CrazylightBoostLow2016/B54172_FM_Ecom.jpg./.resources/topic/adidas/CrazylightBoostLow2016/B54172_SL_eCom.jpg\r', 10),
(14, 'D Rose 7\r', 'These men basketball shoes are cut high for ankle stability and have mesh in the upper for ventilation. A grippy outsole delivers solid traction to weave through traffic, and a moulded collar gives a dynamic fit, even with a brace.\r', '7, 7.5, 8, 8.5, 9, 9.5, 10, 10.5, 11, 11.5, 12, 13', 7995, 0, '0', 'resources/topic/adidas/DRose7/B54134_BL_eCom.jpg./.resources/topic/adidas/DRose7/B54134_FM_eCom.jpg./.resources/topic/adidas/DRose7/B54134_SL_ecom.jpg\r', 4),
(15, 'Pure Boost X (Women)\r', 'With a full boost™ midsole that keeps every stride charged with light, fast energy, the shoe has an upper that hovers over a free-floating arch.\r', '4, 4.5, 5, 5.5, 6, 6.5, 7\r', 5795, 0, '0', 'resources/topic/adidas/PureBoostX/AQ3401_FM_eCom.jpg./.resources/topic/adidas/PureBoostX/AQ3401_SL_eCom.jpg./.resources/topic/adidas/PureBoostX/AQ3401_TPP_eCom.jpg\r', 2),
(16, 'Stan Smith\r', 'Named after tennis legend Stan Smith, the clean, minimalist design gets a new spin with a full grain leather upper that shows off unique woven details. The shoes are built for comfort with a lightweight and airy cork sockliner that complements the ventilating quality of the perforated 3-Stripes.\r', '5, 5.5, 6, 8.5, 11\r', 4895, 0, '0', 'resources/topic/adidas/StanSmith/S76582_BL_eCom.jpg./.resources/topic/adidas/StanSmith/S76582_SL_eCom.jpg./.resources/topic/adidas/StanSmith/S76582_TPP_eCom.jpg\r', 8),
(17, 'Tubular Radial\r', 'Remixing a 90s running vibe for today street style, these shoes are built with a dynamic upper made in textile with neoprene and suede overlays and styled for a contoured, sock-like fit. The tyre-inspired Tubular outsole is updated with a ripple effect.\r', '9.5, 10, 11.5, 12\r', 6495, 0, '0', 'resources/topic/adidas/Tubular(radial)/c0d2187657522b1e9929d5563f4538d0.jpg./.resources/topic/adidas/Tubular(radial)/S80113_BL_eCom.jpg./.resources/topic/adidas/Tubular(radial)/S80113_FM_eCom.jpg\r', 3),
(18, 'Superstar Triple\r', 'The adidas Superstar sneaker reigns supreme. The fan favourite launched in 1970 and soon lived up to its name as NBA players laced into the now-famous design. These shoes update the look in bold monochrome style, featuring a textile upper and matching 3-Stripes, shell toe and outsole.\r', '4.5, 5, 6, 9\r', 4795, 0, '0', 'resources/topic/adidas/SuperstarTriple/BB3695_BL_eCom.jpg./.resources/topic/adidas/SuperstarTriple/BB3695_FM_eCom.jpg./.resources/topic/adidas/SuperstarTriple/BB3695_SL_eCom.jpg\r', 7),
(19, 'Asics Gel Lyte iii\r', 'An ASICS GEL-Lyte III has emerged dressed in matte hues. Rather than lay flat like a desert horizon, the Blue Mirage and sandy tan pop out from their tonal variants. The Blue Mirage is accompanied by a heavier blue on the Tiger Stripes, and the tan by a lighter beige around the sock liner and split tongue.\r', '8,8.5,9.5,10,11,11.5\r', 4350, 5000, '1', 'resources/topic/asics/gel_lyte_iii/asics_gel_lyte_iii_blue_mirage_1.jpg./.resources/topic/asics/gel_lyte_iii/asics_gel_lyte_iii_blue_mirage_2.jpg./.resources/topic/asics/gel_lyte_iii/asics_gel_lyte_iii_blue_mirage_3.jpg\r', 7),
(20, 'Asics Gel Lyte Runner\r', 'The slick GEL-Lyte Runner sees a transformation in color with new take on the classic shoe. The new "Chameleoid Mesh" iteration features a color-changing appearance on the mesh upper which gives off a different look every step depending on the viewing angle.\r', '7,7.5,8,8.5,9,9.5,10\r', 4480, 0, '1', 'resources/topic/asics/gel_lyte_runner_chameleon/asics_gel_lyte_runner_chameleon_1.jpg./.resources/topic/asics/gel_lyte_runner_chameleon/asics_gel_lyte_runner_chameleon_2.jpg./.resources/topic/asics/gel_lyte_runner_chameleon/asics_gel_lyte_runner_chameleon_3.jpg\r', 2),
(21, 'Asics Gel Lyte III Oreo Pack\r', 'Mocked up in a simple but endearing "Oreo" colorway is the latest rework of its coveted GEL-Lyte III model. The cookie-inspired color scheme is available in two colorways.\r', '8,8.5,9,9.5,10,10.5,12\r', 6700, 6732, '1', 'resources/topic/asics/asics_gel_lyte_iii_oreo_pack/asics_gel_lyte_iii_oreo_pack_03.jpg./.resources/topic/asics/asics_gel_lyte_iii_oreo_pack/asics_gel_lyte_iii_oreo_pack_04.jpg./.resources/topic/asics/asics_gel_lyte_iii_oreo_pack/asics_gel_lyte_iii_oreo_pack_05.jpg\r', 4),
(22, 'AsicsGel Lyte III Japanese Textile Sneaker\r', 'We get a closer look at ASICS ravishing new polka dot-themed GEL-Lyte III, officially dubbed the "Japanese Textile" edition. Paying homage to the company’s longstanding Japanese roots, this iconic GEL-Lyte III model gets treated with high quality Japanese selvedge denim for that extra premium touch.\r', '7,8,9.5,10,11,12\r', 6700, 0, '1', 'resources/topic/asics/asics_gel_lyte_iii_japanese_textile_sneaker/asics_gel_lyte_iii_japanese_textile_sneaker_2.jpg./.resources/topic/asics/asics_gel_lyte_iii_japanese_textile_sneaker/asics_gel_lyte_iii_japanese_textile_sneaker_6.jpg./.resources/topic/asics/asics_gel_lyte_iii_japanese_textile_sneaker/asics_gel_lyte_iii_japanese_textile_sneaker_7.jpg\r', 1),
(23, 'Ronnie Fieg Kith Asics Gel Lyte III 3-1 Super Gree', 'A couple of weeks ago, Ronnie Fieg announced that he’d be bringing back the elusive "Super Green" edition of the GEL-Lyte III in honor of KITH’s fifth anniversary. Originally released back in 2013, the kicks were created alongside footwear-centric charity Soles4Souls and limited to just 300 pairs that were given away to those less fortunate in Haiti. Now Fieg has officially unveiled the release, as well as the first-ever GEL-Lyte 3.1 that he promised alongside it.\r', '8,8.5,9.5,10,10.5,11,11.5\r', 8800, 0, '1', 'resources/topic/asics/ronnie_fieg_kith_asics_gel_lyte_iii_3_1_super_green/ronnie_fieg_kith_asics_gel_lyte_iii_3_1_super_green_1.jpg./.resources/topic/asics/ronnie_fieg_kith_asics_gel_lyte_iii_3_1_super_green/ronnie_fieg_kith_asics_gel_lyte_iii_3_1_super_green_4.jpg./.resources/topic/asics/ronnie_fieg_kith_asics_gel_lyte_iii_3_1_super_green/ronnie_fieg_kith_asics_gel_lyte_iii_3_1_super_green_5.jpg\r', 5),
(24, 'Atmos Asics Gel Lyte V Black Gold\r', 'Back in August of 2011, Japanese premium sneaker boutique atmos released a beautiful Black/Gold colorway for its collaborative GEL-Lyte III. This 15th anniversary year, the shop is working with ASICS once again, transferring the clean and semi-subtle colorway onto the sock-like GEL-Lyte V. A black overall silhouette covers the shoe while hits of gold line the lateral and medial ASICS striping and font throughout. A speckled midsole adds a bit of flair while the black leather, suede and mesh materials combine sportiness with luxury. Look for these to pre-sale at atmos on December 5 with a general release later on December 19 for approximately $140 USD.\r', '7,8,9.5,10,11,12\r', 6160, 0, '1', 'resources/topic/asics/atmos_asics_gel_lyte_v_black_gold/atmos_asics_gel_lyte_v_black_gold_1.jpg./.resources/topic/asics/atmos_asics_gel_lyte_v_black_gold/atmos_asics_gel_lyte_v_black_gold_2.jpg./.resources/topic/asics/atmos_asics_gel_lyte_v_black_gold/atmos_asics_gel_lyte_v_black_gold_4.jpg\r', 4),
(25, 'BAIT Helps ASICS Celebrate 25 Years of the GEL-Lyt', 'ASICS has tapped BAIT for a pristine celebration. Commemorating the split tongue’s 25-year benchmark, the GEL-Lyte III has been chosen as the canvas. Lavish materials such as reptile and tilapia skin cover the shoe’s upper along with hexagonal perforations and reflective silver paneling. Additionally, the mix of sport and luxury is what makes the upcoming runner stand out among the rest. This special edition GEL-Lyte II will release exclusively at BAIT stores via in-store raffle on September 26. An online raffle will be held also.\r', '7,7.5,9.5,10,11,12\r', 7000, 0, '1', 'resources/topic/asics/bait_asics_gel_lyte_iii_25th_anniversary/bait_asics_gel_lyte_iii_25th_anniversary_1.jpg./.resources/topic/asics/bait_asics_gel_lyte_iii_25th_anniversary/bait_asics_gel_lyte_iii_25th_anniversary_2.jpg./.resources/topic/asics/bait_asics_gel_lyte_iii_25th_anniversary/bait_asics_gel_lyte_iii_25th_anniversary_3.jpg\r', 1),
(26, 'Air Footscape Chukka\r', 'Built with a primarily woven upper and leather accents on the eyestay and heel tab, the Nike Air Footscape Desert Chukka combines a bold look with timeless comfort in a mid-cut profile. The design’s lightweight woven construction is further complimented with the natural motion Footscape tooling to ensure flexable comfort while hustling through busy city-streets.\r', '7,7.5,9.5,10,11,12\r', 8500, 8610.39, '2', 'resources/topic/nike/Air_Footscape_Chukka/Air_Footscape_Chukka(1).jpg./.resources/topic/nike/Air_Footscape_Chukka/Air_Footscape_Chukka(2).jpg./.resources/topic/nike/Air_Footscape_Chukka/Air_Footscape_Chukka(3).jpg\r', 3),
(27, 'The Nike Air Huarache Ultra Black White\r', 'The Nike Air Huarache Ultra Men Shoe features a perforated foam and mesh upper and midsole-outsole combo for enhanced breathability and lightweight comfort. An elastic strap and rubber cage on the heel delivers a classic Huarache look.\r', '7,7.5,9.5,10,11,12\r', 4539.59, 5740.26, '2', 'resources/topic/nike/huarache/nike_air_huarache_run_ultra-black_white_1_1.jpg./.resources/topic/nike/huarache/nike_air_huarache_run_ultra-black_white_1_2.jpg./.resources/topic/nike/huarache/W-AIR-HUARACHE-RUN-ULTRA-819151_001_A_PREM.jpg\r', 2),
(28, 'NIKE ROSHE TWO FLYKNIT\r', 'Simplicity redefined, the Nike Roshe Two Flyknit Shoe features breathable Nike Flyknit material and three layers of cushioning for soft comfort.\r', '7,7.5,9.5,10,11,12\r', 6212.11, 0, '2', 'resources/topic/nike/Nike_Roshi_Two_Lyknit/Nike_Roshi_Two_Lyknit1.jpg./.resources/topic/nike/Nike_Roshi_Two_Lyknit/Nike_Roshi_Two_Lyknit2.jpg./.resources/topic/nike/Nike_Roshi_Two_Lyknit/Nike_Roshi_Two_Lyknit3.jpg\r', 7),
(29, 'Nike Air Force 1\r', 'Shoe updates a hoops original with grid top-stitching for unforgettable style and a classic, court-inspired silhouette.\r', '7,7.5,9,10,11,12\r', 5017.9, 0, '2', 'resources/topic/nike/Nike_Air_Force_1_07_Mid_LV8/Nike_Air_Force_1_07_Mid_LV8(1).jpg./.resources/topic/nike/Nike_Air_Force_1_07_Mid_LV8/Nike_Air_Force_1_07_Mid_LV8(2)./.resources/topic/nike/Nike_Air_Force_1_07_Mid_LV8/Nike_Air_Force_1_07_Mid_LV8(3).jpg\r', 1),
(30, 'Shadow Original Black\r', 'Padded collar and tongue delivers a comfy fit. Comfortable fabric lining and a cushioned EVA sockliner for all-day comfort. Traditional lace up front provides optimum fit. "Marshmallowy" EVA midsole cushions and absorbs shock. TPU heel piece provides extra support. Classic Saucony Originals triangular rubber tread pattern.\r', '7,7.5,8,9,9.5,10,10.5,11\r', 3380, 0, '4', 'resources/topic/saucony/Shadow_Original_Black/2108_518_1_1200x735.jpg./.resources/topic/saucony/Shadow_Original_Black/2108_518_2_1200x735.jpg./.resources/topic/saucony/Shadow_Original_Black/2108_518_4_1200x735.jpg\r', 2),
(31, 'Jazz Original Charcoal White\r', 'Lace-up front with a padded collar and tongue Nylon/suede upper for long wear Padded tongue, collar, and footbed provides superior shock absorption, fit, and extra comfort.\r', '7,8,9,9.5,10,10.5,11,12\r', 2420, 0, '4', 'resources/topic/saucony/Jazz_Original_Charcoal_White/2044_274_1_1200x735.jpg./.resources/topic/saucony/Jazz_Original_Charcoal_White/2044_274_2_1200x735.jpg./.resources/topic/saucony/Jazz_Original_Charcoal_White/2044_274_4_1200x735.jpg\r', 1),
(32, 'Running Lancer\r', 'Heel PROGRID and medial support arc provide premium cushioning and a touch of support. New midfoot weld provides lightweight lockdown.\r', '7,7.5,8,9,9.5,10,10.5,11\r', 2639.56, 3520, '4', 'resources/topic/saucony/Running_Lancer_2/S25264_2_1.jpg./.resources/topic/saucony/Running_Lancer_2/S25264_2_2.jpg./.resources/topic/saucony/Running_Lancer_2/S25264_2_4.jpg\r', 9),
(33, 'Grid Life on the Run\r', 'Almost three years in the making, the GRID SD running silhouette and technology marked the first time that a system simultaneously absorbed shock and provide stability for runners during their training. Originally introduced in 1991, this silhouette became an excellent performance trainer for all types of runners.\r', '7,8,9,9.5,10,10.5,11,12\r', 3960, 0, '4', 'resources/topic/saucony/Grid_Life_On_The Run/S70274_1_1.jpg./.resources/topic/saucony/Grid_Life_On_The Run/S70274_1_2.jpg./.resources/topic/saucony/Grid_Life_On_The Run/S70274_1_4.jpg\r', 1),
(34, 'Bullet Black\r', 'Nylon upper with suede overlays is comfortable and stylish, the perfect combination Low profile silhouette and cross-country platform creates an authentic retro feel.\r', '7,7.5,8,9,9.5,10,10.5,11\r', 2420, 0, '4', 'resources/topic/saucony/Bullet/2943_6_1_1200x735.jpg./.resources/topic/saucony/Bullet/2943_6_2_1200x735.jpg./.resources/topic/saucony/Bullet/2943_6_4_1200x735.jpg\r', 6),
(35, 'la-mjc diadora all gone n9000\r', 'To help commemorate the one year anniversary milestone of its flagship store, Club 75 is set to unveil five new sneaker releases during this season’s Paris Fashion Week. Paying homage to the book “All Gone," a street-wise narrative on the best streetwear finds of the previous 12-months, we see here the first drop from the La MJC and Diadora alliance, called the “All Gone" Diadora N9000. Drawing direct design cues from the fourth release published in 2009; the new silhouette gets a clean white upper, constructed from a premium soft leather/mesh combo, with lively red accents lining the base, sole, and laces. \r', '7,7.5,8,9,9.5,10,10.5,11\r', 7371.53, 8845.84, '3', 'resources/topic/diadora/la_mjc_all_gone_n9000/la-mjc-all-gone-diadora-n9000-1.jpeg./.resources/topic/diadora/la_mjc_all_gone_n9000/la-mjc-all-gone-diadora-n9000-4.jpg./.resources/topic/diadora/la_mjc_all_gone_n9000/la-mjc-all-gone-diadora-n9000-5.jpg\r', 0),
(36, 'Dashhex TR Training Shoes \r', 'Showcase your full potential with the DashHex TR from Reebok. The upper is constructed with PU-coated overlays, while the midsole flaunts 3D Ultralite EVA foam to provide improved cushioning and stability. - PU coated leather overlays- Low-cut design- 3D Ultralite EVA foam midsole for lightweight cushioning and stability- Midsole cradle for stabilizing medial and lateral movements- Low-profile, one-piece mid- to outsole with rubber for traction- Forefoot flex grooves- Memory Tech sockliner.\r', '6,7,7.5,8,8.5\r', 3195, 0, '5', 'resources/topic/reebok/Dashhex_TR_Training_Shoes/reebok_5215_819246_2.jpg./.resources/topic/reebok/Dashhex_TR_Training_Shoes/reebok_5220_819246_4.jpg./.resources/topic/reebok/Dashhex_TR_Training_Shoes/reebok_5224_819246_6.jpg\r', 3),
(37, 'Hayasu Training Shoes\r', 'Cultivate your own dance daring drive in these Hayasu from Reebok. It has been engineered to support your foot through a full range of movement with its 3D Ultralite cushions feet from impacts. It features Specialised TurnZone and grooves on the sole keep feet light while maximizing metatarsal flexibility to keep you dancing. - Seamless neoprene bootie construction - Mid-cut shape for extra ankle support and improved stability- 3D Ultralite midsole foam - Medial and lateral soft thermoplastic polyurethane pieces for midfoot support- Minimal rubber sole- Moulded Ortholite sockliner.\r', '6,6.7,7,7.5,8,8.5,9\r', 5295, 0, '5', 'resources/topic/reebok/Hayasu_Training_Shoes/reebok_5198_919246_2.jpg./.resources/topic/rebook/Hayasu_Training_Shoes/reebok_5204_919246_4.jpg./.resources/topic/reebok/Hayasu_Training_Shoes/reebok_5209_919246_6.jpg\r', 8),
(38, 'Hexaffect Sport Running Shoes\r', 'Your marathon performance will improve if you have a dependable footwear like the Hexaffect Sport Running Shoes from Reebok. Designed to improve brathability and cushioning, Reebok equipped this pair with a lightweight mesh upper and hexagon-shaped flex nodes on the midsole.- Lightweight mesh upper with a midfoot cage- Low-cut design- Hexagon shaped flex nodes on the midsole for natural foot flexibility- Unique mesh collar at the heel for added comfort- Rubber pods on the outsole- 3-D effect on the side for visual dimension- Best for long distance running, Ragnar Relay Races.\r', '7,8,9,9.5,10,11,12\r', 3495, 0, '5', 'resources/topic/reebok/Hexaffect_Sport_Running_Shoes/reebok_6786_886246_2.jpg./.resources/topic/reebok/Hexaffect_Sport_Running_Shoes/reebok_6789_886246_4.jpg./.resources/topic/reebok/Hexaffect_Sport_Running_Shoes/reebok_6792_886246_6.jpg\r', 5),
(39, 'Sublite XT Cushion MT Running Shoes\r', 'The Sublite XT Running Shoes from Reebok will easily keep up with your pace. The pair is constructed with sandwich mesh upper to give you maximum breathability and support.- Sandwich mesh upper- Low-cut design for easy ankle mobility and quick stride transitions- New foam compound technology- Full-foot flex grooves- Rubber compound added to the outsole in the heel and forefoot for traction- 3D FuseFrame toecap and vamp for a seamless and lightweight running detail- Upper materials and graphics.\r', '9,9.5,10,10.5,11,12\r', 3795, 0, '5', 'resources/topic/reebok/Sublite_XT_Cushion_MT_Running_Shoes/reebok_8908_076745_2.jpg./.resources/topic/reebok/Sublite_XT_Cushion_MT_Running_Shoes/reebok_8907_076745_1.jpg./.resources/topic/reebok/Sublite_XT_Cushion_MT_Running_Shoes/reebok_8910_076745_3.jpg\r', 0),
(40, 'TrailGrip RS 5.0 Outdoor Shoes\r', 'Walk every terrain with the Trailgrip RS 4.0 shoes from Reebok. The upper is constructed with mesh and high-abrasion synthetic details to protect your feet, while the sole is designed with rubber active traction lug to give you better support and grip.- Mesh and high-abrasion synthetic details for breathable, protective durability- Low-cut design for freedom of motion at ankle and quicker stride transitions- DMX Ride tech transfers air within the foam outsole for super shock absorption- Microweb lacing and custom tongue loop for a locked-in fit and trail-friendly style- Rubber active traction lug outsole for gritty grip from street to summit- EVA midsole with stability bar in forefoot for confident steadiness in all terrains.\r', '8,8.5,9,9.5,10\r', 3995, 0, '5', 'resources/topic/reebok/TrailGrip_RS_5.0_Outdoor_Shoes/reebok_2398_748526_2.jpg./.resources/topic/reebok/TrailGrip_RS_5.0_Outdoor_Shoes/reebok_2397_748526_1.jpg./.resources/topic/reebok/TrailGrip_RS_5.0_Outdoor_Shoes/reebok_240_748526_6.jpg\r', 10),
(41, 'Adidas NMD XR1\r', 'Flaunting its defining primeknit construction in a mixture of blue and grey tones, more tonal blue hits adorn the signature TPU lacing cage apparatus across its side panels, alongside respective branding on the tongue and rear. In addition, pops of red are utilized for its heel pull tabs, while a full-length boost sole unit in white finishes off the profile.\r', '8,8.5,9,9.5,10\r', 11704, 0, '0', 'resources/topic/adidas/nmd_xr1/nmd_xr1_1.PNG./.resources/topic/adidas/nmd_xr1/nmd_xr1_2.PNG./.resources/topic/adidas/nmd_xr1/nmd_xr1_3.PNG\r', 10),
(42, 'Shadow 6000 Premium Aquamarine\r', 'Electric teal, rich purple and vivid cerulean blue draw inspiration from the coloring used to dye eggs, it is a classic Easter intensified - Lace-up front with a padded collar and tongue - Suede upper for long wear\r', '7,7.5,8,9,9.5,10,10.5,11\r', 4344.8, 0, '4', 'resources/topic/saucony/Shadow_6000_aquamarine/Shadow_6000_blue_1.jpg./.resources/topic/saucony/Shadow_6000_aquamarine/Shadow_6000_blue_2.jpg./.resources/topic/saucony/Shadow_6000_aquamarine/Shadow_6000_blue_3.jpg\r', 5);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_tbl`
--

CREATE TABLE `transaction_tbl` (
  `trans_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL,
  `payment_method` char(5) NOT NULL,
  `trans_amount` double NOT NULL,
  `trans_payment` double NOT NULL,
  `trans_date` datetime NOT NULL,
  `trans_ip` char(10) NOT NULL,
  `exp_del_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_tbl`
--

CREATE TABLE `user_tbl` (
  `user_id` int(11) NOT NULL,
  `email` varchar(30) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `contactNo` char(11) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(30) NOT NULL,
  `zipcode` char(5) NOT NULL,
  `cardNo` varchar(10) NOT NULL,
  `expiryDate` date NOT NULL,
  `cardHolderName` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `username` varchar(30) NOT NULL,
  `age` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_tbl`
--

INSERT INTO `user_tbl` (`user_id`, `email`, `firstname`, `lastname`, `contactNo`, `address`, `city`, `zipcode`, `cardNo`, `expiryDate`, `cardHolderName`, `password`, `username`, `age`) VALUES
(3, 'moribaleta@gmail.com', '', '', '1234567', '#36.pound St. Baranka', 'Marikina City', '1803', '123456', '0000-00-00', '12312312', '12345', 'moribaleta', '1997-10-30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brand_tbl`
--
ALTER TABLE `brand_tbl`
  ADD PRIMARY KEY (`brand_id`);

--
-- Indexes for table `cart_detail`
--
ALTER TABLE `cart_detail`
  ADD PRIMARY KEY (`cart_detail_id`);

--
-- Indexes for table `cart_tbl`
--
ALTER TABLE `cart_tbl`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `product_tbl`
--
ALTER TABLE `product_tbl`
  ADD PRIMARY KEY (`prod_id`);

--
-- Indexes for table `user_tbl`
--
ALTER TABLE `user_tbl`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brand_tbl`
--
ALTER TABLE `brand_tbl`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `cart_detail`
--
ALTER TABLE `cart_detail`
  MODIFY `cart_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `cart_tbl`
--
ALTER TABLE `cart_tbl`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `product_tbl`
--
ALTER TABLE `product_tbl`
  MODIFY `prod_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `user_tbl`
--
ALTER TABLE `user_tbl`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
