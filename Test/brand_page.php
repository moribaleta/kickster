<?php

if($_GET){
    $brand_id = $_GET['temp_storage_brand'];
    $content = openBrandItems($brand_id);
    require('resources/php/connection.php');
    $sql = "select * from brand_tbl where brand_id='$brand_id'";
    $result = $con->query($sql);
    if($result->num_rows>0){
        $row = $result->fetch_assoc();  
        $img_logo = explode("./.",$row['brand_logo']);
        echo "<html>

        <head>

            <title>".$row['brand_name']."</title>            
            <link rel='stylesheet' href='resources/css/brand.css'>
            <link rel='stylesheet' href='resources/css/brand_content.css'>            
            <script src='resources/js/jquery-3.0.0.min.js'></script>
            <script src='resources/js/brands.js'></script>
            <script src='resources/js/storage.js'></script>
            <script src='resources/js/product.js'></script>       
            <script src='resources/js/html_php_access.js'></script>
        </head>

        <body>

            <div id= 'external'>

                <div id = 'header' class='header'>
                    <div id='header_logo' href='index.html'>
                        <a href='index.html'>
                            <img src='resources/images/bannerlogoblack.png'   >
                        </a>
                       <div id='nav_header'>
                            <a href='index.html'>home</a>
                            <a href='sale.php'>sale</a>
                            <a href='brand.php'>brands</a>
                            <a href='About_us.html'>about us</a>
                            <a href='cart.html'>cart</a>
                            <a href='login.php'>login</a>
                        </div>
                    </div>

                </div>

                <div id = 'con'>
                    <div id='homepage'>
                        <div id='foreground' class='foreground'>
                            <a href='#container' id='bottom'title='click me to scroll down' >
                                <img id='brand_logo' src='".$img_logo[1]."'>
                            </a>
                        </div>

                        <div id='background'>
                            <img id='brand_wallpaper' src='".$row['brand_img_src']."'>
                        </div>

                    </div>

                    <div id='row'>
                        <br>
                        <br>
                        <br>
                        ".$content."
                    </div>
                    <div id = 'footer'>
                        <img src='resources/images/iconwhite.png' >
                        <a>Copyright 2016 PUP</a>
                    </div>
                    <div id='row'></div>

                </div>

            </div> 
     
        </body>

    </html>";
    }else{
        echo "error";
    }
    $con->close();
}

function openBrandItems($brand_id){   
    require('resources/php/connection.php');

    $sql = "select * from product_tbl where brand_id = '$brand_id'";    

    $result = $con->query($sql);

    if($result->num_rows>0){
        $catch = "";
        while($row = $result->fetch_assoc()){                         
            $img = explode("./.",$row["prod_img_src"]);
            if($row["prod_price_before"]>0){
                $catch .= "<div id = 'container' class='fade'> <div id='info'>
                "."<h2>".$row["prod_name"]."</h2>
                "."<strike>Php.".$row["prod_price_before"]."</strike><br>
                "."<price>Php.".$row["prod_price"]."</price><br><br>
                "."<a target = '_parent'
                "."onclick='openItem(".$row["prod_id"].")'>
                "."buy me
                "."</a>
                "."</div>
                "."<img src='".$img[0]."' >
                "."</div>";
            }else{
                $catch .= "<div id = 'container' class='fade'> <div id='info'>
                "."<h2>".$row["prod_name"]."</h2>                
                "."<price>Php.".$row["prod_price"]."</price><br><br>
                "."<a target = '_parent'
                      "."onclick='openItem(".$row["prod_id"].")'>
                "."buy me
                "."</a>
                "."</div>
                "."<img src='".$img[0]."' >
                "."</div>";
            }                        
        }
    } 
    $con->close();
    return $catch;
}




?>