Shadow Original Black
Padded collar and tongue delivers a comfy fit. Comfortable fabric lining and a cushioned EVA sockliner for all-day comfort. Traditional lace up front provides optimum fit. "Marshmallowy" EVA midsole cushions and absorbs shock. TPU heel piece provides extra support. Classic Saucony Originals triangular rubber tread pattern.
in stocks
3380.00
7,7.5,8,9,9.5,10,10.5,11
resources/topic/saucony/Shadow_Original_Black/2108_518_1_1200x735.jpg./.resources/topic/saucony/Shadow_Original_Black/2108_518_2_1200x735.jpg./.resources/topic/saucony/Shadow_Original_Black/2108_518_4_1200x735.jpg
Saucony
-----
Jazz Original Charcoal White
Lace-up front with a padded collar and tongue Nylon/suede upper for long wear Padded tongue, collar, and footbed provides superior shock absorption, fit, and extra comfort.
in stocks
2420
7,8,9,9.5,10,10.5,11,12
resources/topic/saucony/Jazz_Original_Charcoal_White/2044_274_1_1200x735.jpg./.resources/topic/saucony/Jazz_Original_Charcoal_White/2044_274_2_1200x735.jpg./.resources/topic/saucony/Jazz_Original_Charcoal_White/2044_274_4_1200x735.jpg
Saucony
-----
Running Lancer
Heel PROGRID and medial support arc provide premium cushioning and a touch of support. New midfoot weld provides lightweight lockdown.
in stocks
3520.00->2639.56
7,7.5,8,9,9.5,10,10.5,11
resources/topic/saucony/Running_Lancer_2/S25264_2_1.jpg./.resources/topic/saucony/Running_Lancer_2/S25264_2_2.jpg./.resources/topic/saucony/Running_Lancer_2/S25264_2_4.jpg
Saucony
-----
Grid Life on the Run
Almost three years in the making, the GRID SD running silhouette and technology marked the first time that a system simultaneously absorbed shock and provide stability for runners during their training. Originally introduced in 1991, this silhouette became an excellent performance trainer for all types of runners.
in stocks
3960
7,8,9,9.5,10,10.5,11,12
resources/topic/saucony/Grid_Life_On_The Run/S70274_1_1.jpg./.resources/topic/saucony/Grid_Life_On_The Run/S70274_1_2.jpg./.resources/topic/saucony/Grid_Life_On_The Run/S70274_1_4.jpg
Saucony
-----
Bullet Black
Nylon upper with suede overlays is comfortable and stylish, the perfect combination Low profile silhouette and cross-country platform creates an authentic retro feel.
in stocks
2420
7,7.5,8,9,9.5,10,10.5,11
resources/topic/saucony/Bullet/2943_6_1_1200x735.jpg./.resources/topic/saucony/Bullet/2943_6_2_1200x735.jpg./.resources/topic/saucony/Bullet/2943_6_4_1200x735.jpg
Saucony
-----
Shadow 6000 Premium Aquamarine
Electric teal, rich purple and vivid cerulean blue draw inspiration from the coloring used to dye eggs � it's classic Easter intensified - Lace-up front with a padded collar and tongue - Suede upper for long wear
in stocks
4344.80
7,7.5,8,9,9.5,10,10.5,11
resources/topic/saucony/Shadow_6000_aquamarine/Shadow_6000_blue_1.jpg./.resources/topic/saucony/Shadow_6000_aquamarine/Shadow_6000_blue_2.jpg./.resources/topic/saucony/Shadow_6000_aquamarine/Shadow_6000_blue_3.jpg
Saucony