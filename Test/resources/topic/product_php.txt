
N9000 MM
The iconic N9000 revisited, using performance materials in the upper. The outsole faithfully reproduces the original construction, making the shoe comfortable.
7235.53->7000.00
7,7.5,9.5,10,11,12
resources/topic/diadora/diadora_N9000_Black/diadora_N9000_MM(1).jpg./.resources/topic/diadora/diadora_N9000_Black/diadora_N9000_MM(2).jpg./.resources/topic/diadora/diadora_N9000_Black/diadora_N9000_MM(3).jpg
diadora
-----
ASICS GEL-Lyte III "Black Mamba"
The ASICS GEL-Lyte III "Black Mamba" is a reptilian-inspired sneaker that offers a darker alternative to the loud, colorful sneakers being released this season. Showcased in a triple-black colorway, the sneaker sports scaly construction throughout with branding texturally juxtaposed in black suede and embossed on the midsole and heel counter. Minimal hints of white in the laces provide the only offset in this offering. A release date has not been confirmed but the ASICS GEL-Lyte III "Black Mamba" will retail for $130 USD and you can pick up a pair via Oneness� online store this August.
5720.00
8,8.5,9.5,10,10.5,11,11.5
resources/topic/asics/asics_gel_lyte_iii_black_mamba/asics_gel_lyte_iii_black_mamba_0001.jpg./.resources/topic/asics/asics_gel_lyte_iii_black_mamba/asics_gel_lyte_iii_black_mamba_02.jpg./.resources/topic/asics/asics_gel_lyte_iii_black_mamba/asics_gel_lyte_iii_black_mamba_03.jpg
Asics
-----
Trident Brogue St Moritz
The Trident Brogue Saint Moritz features a luxurious suede upper with Oxford detailing, for a hand-crafted, elegant effect that is perfectly suited to any type of occasion.
9346.46->9000.00
7,7.5,9.5,10,11,12
resources/topic/diadora/Trident_Brogue_St_Moritz/Trident_Brogue_St_Moritz(1).jpg./.resources/topic/diadora/Trident_Brogue_St_Moritz/Trident_Brogue_St_Moritz(2).jpg./.resources/topic/diadora/Trident_Brogue_St_Moritz/Trident_Brogue_St_Moritz(3).jpg
diadora
-----
NIKE AIR MAX 1 ULTRA FLYKNIT
The Air Max 1 gets remixed with breathable Flyknit for the first time. A cored-out Ultra midsole and plush Air Max cushioning complete the sneaker, delivering featherweight, all-day comfort.
7653.68
7,7.5,9.5,10,11,12
resources/topic/nike/NIKE_AIR_MAX_1_ULTRA_FLYKNIT/NIKE_AIR_MAX_1_ULTRA_FLYKNIT(1).jpg./.resources/topic/nike/NIKE_AIR_MAX_1_ULTRA_FLYKNIT/NIKE_AIR_MAX_1_ULTRA_FLYKNIT(2).jpg./.resources/topic/nike/NIKE_AIR_MAX_1_ULTRA_FLYKNIT/NIKE_AIR_MAX_1_ULTRA_FLYKNIT(3).jpg
Nike
-----
Mi Basket Power
A Diadora icon developed in 1984 to celebrate the brand sponsorship of the Milan basketball team. This time around, the model is revisited with a high-fashion, luxury touch, in full grain leather with a gloss-effect wax finish, available in two stylish colours.
4849.74
7,7.5,9.5,10,11,12
resources/topic/diadora/Mi_Basker_Power/Mi_Basket_Power(1).jpg./.resources/topic/diadora/Mi_Basker_Power/Mi_Basket_Power(2).jpg./.resources/topic/diadora/Mi_Basker_Power/Mi_Basket_Power(3).jpg
diadora
-----
ZX Flux ADV X
The ZX family of running shoes caught the attention of high-mileage runners around 1984 with a futuristic look and feel. .These shoes are crafted in premium nubuck with a single-piece fold-over tongue construction. Welded trim and a neoprene heel cage give it a touch of technical style.
6495.00 
4.5, 5, 6, 6.5, 7.5, 11
resources/topic/adidas/ZXfLuxADVX/S76364_BL_eCom.jpg./.resources/topic/adidas/ZXfLuxADVX/S76364_FM_eCom.jpg./.resources/topic/adidas/ZXfLuxADVX/S76364_SL_eCom.jpg
Adidas
-----
B.elite Liquid
Stylish interpretation in quality full-grain leather of the Diadora model which has had the greatest success in the most famous tennis tournaments in the world. Model embellished with inner �starry" padding, which lends the shoe a creative touch.
6384.79->6200.00
8,8.5,9.5,10,11,11.5
resources/topic/diadora/B.elite_Liquid/B.elite_Liquid(1).jpg./.resources/topic/diadora/B.elite_Liquid/B.elite_Liquid(2).jpg./.resources/topic/diadora/B.elite_Liquid/B.elite_Liquid(3).jpg
diadora
-----
Exodus Nyl
The Exodus Nyl model is inspired by the 90s and has a nylon upper with suede trims. The special vintage waxing lends this shoe a sporty and vintage feel at the same time.
9073.72->8500.00
8,8.5,9.5,10,11,11.5
resources/topic/diadora/Exodus_Nyl/Exodus_Nyl(1).jpg./.resources/topic/diadora/Exodus_Nyl/Exodus_Nyl(2).jpg./.resources/topic/diadora/Exodus_Nyl/Exodus_Nyl(3).jpg
diadora
-----
Ultraboost
ULTRA BOOST SHOES COMFORTABLE ENERGY-RETURNING SHOES MADE FOR NATURAL RUNNING, RAIN OR SHINE.
7000.00
8,8.5,9.5,10,11,11.5
resources/topic/adidas/ultraboost/front_up_ultraboost.jpg./.resources/topic/adidas/ultraboost/sideface.jpg./.resources/topic/adidas/ultraboost/soleface.jpg
Adidas
-----
Crazy Explosive
They pair a textile and synthetic upper with a full-length boost� midsole that offers energy return in every step.Designed to work with the muscles, bones and tendons of the foot, the unique lace system provides an enhanced fit.
7995.00
9, 9.5
resources/topic/adidas/CrazyExplosive/B42424_BL_eCom.jpg./.resources/topic/adidas/CrazyExplosive/B42424_FM_eCom.jpg./.resources/topic/adidas/CrazyExplosive/B42424_SL_eCom.jpg
Adidas
-----
Crazy Explosive Primeknit
Explode past the defence in these men multicoloured basketball shoes. With a flexible adidas Primeknit upper, they feature a full-length boost� midsole for ultimate energy return and a unique lacing system for an enhanced fit.
8795.00
7.5, 8, 8.5, 9, 9.5, 10, 10.5
resources/topic/adidas/CrazyExplosivePrimeKnit/AQ7218_BL_eCom.jpg./.resources/topic/adidas/CrazyExplosivePrimeKnit/AQ7218_FM_eCom.jpg./.resources/topic/adidas/CrazyExplosivePrimeKnit/AQ7218_LSL_eCom.jpg
Adidas
-----
D Lillard 2.0
Rising out of Oakland, Damian Lillard emerged as a star player with a tireless work ethic. Designed after his style of play, these men basketball shoes are cut low for ankle mobility with extra support in the forefoot, great for stepback jump shots.
5995.00
7, 7.5, 8, 8.5, 9, 9.5, 10, 10.5, 11, 11.5, 12, 13.5
resources/topic/adidas/DLillard2.0/B42386_BL_eCom.jpg./.resources/topic/adidas/DLillard2.0/B42386_FM_eCom.jpg./.resources/topic/adidas/DLillard2.0/B42386_SL_eCom.jpg
Adidas
-----
Crazy Light Boost Low 2016
Capitalise on the fast break in these men basketball shoes. Designed to burst past the defence, they ride a full-length boost� midsole for energy return with every step.
7995.00
7.5, 8.5, 9, 9.5, 10, 10.5, 11, 11.5, 12, 13.5
resources/topic/adidas/CrazylightBoostLow2016/B54172_BL_eCom.jpg./.resources/topic/adidas/CrazylightBoostLow2016/B54172_FM_Ecom.jpg./.resources/topic/adidas/CrazylightBoostLow2016/B54172_SL_eCom.jpg
Adidas
-----
D Rose 7
These men basketball shoes are cut high for ankle stability and have mesh in the upper for ventilation. A grippy outsole delivers solid traction to weave through traffic, and a moulded collar gives a dynamic fit, even with a brace.
7995.00
7, 7.5, 8, 8.5, 9, 9.5, 10, 10.5, 11, 11.5, 12, 13.5
resources/topic/adidas/DRose7/B54134_BL_eCom.jpg./.resources/topic/adidas/DRose7/B54134_FM_eCom.jpg./.resources/topic/adidas/DRose7/B54134_SL_ecom.jpg
Adidas
-----
Pure Boost X (Women)
With a full boost� midsole that keeps every stride charged with light, fast energy, the shoe has an upper that hovers over a free-floating arch.
5795.00
4, 4.5, 5, 5.5, 6, 6.5, 7
resources/topic/adidas/PureBoostX/AQ3401_FM_eCom.jpg./.resources/topic/adidas/PureBoostX/AQ3401_SL_eCom.jpg./.resources/topic/adidas/PureBoostX/AQ3401_TPP_eCom.jpg
Adidas
-----
Stan Smith
Named after tennis legend Stan Smith, the clean, minimalist design gets a new spin with a full grain leather upper that shows off unique woven details. The shoes are built for comfort with a lightweight and airy cork sockliner that complements the ventilating quality of the perforated 3-Stripes.
4895.00
5, 5.5, 6, 8.5, 11
resources/topic/adidas/StanSmith/S76582_BL_eCom.jpg./.resources/topic/adidas/StanSmith/S76582_SL_eCom.jpg./.resources/topic/adidas/StanSmith/S76582_TPP_eCom.jpg
Adidas
-----
Tubular Radial
Remixing a 90s running vibe for today street style, these shoes are built with a dynamic upper made in textile with neoprene and suede overlays and styled for a contoured, sock-like fit. The tyre-inspired Tubular outsole is updated with a ripple effect.
6495.00
9.5, 10, 11.5, 12
resources/topic/adidas/Tubular(radial)/c0d2187657522b1e9929d5563f4538d0.jpg./.resources/topic/adidas/Tubular(radial)/S80113_BL_eCom.jpg./.resources/topic/adidas/Tubular(radial)/S80113_FM_eCom.jpg
Adidas
-----
Superstar Triple
The adidas Superstar sneaker reigns supreme. The fan favourite launched in 1970 and soon lived up to its name as NBA players laced into the now-famous design. These shoes update the look in bold monochrome style, featuring a textile upper and matching 3-Stripes, shell toe and outsole.
4795.00
4.5, 5, 6, 9
resources/topic/adidas/SuperstarTriple/BB3695_BL_eCom.jpg./.resources/topic/adidas/SuperstarTriple/BB3695_FM_eCom.jpg./.resources/topic/adidas/SuperstarTriple/BB3695_SL_eCom.jpg
Adidas
-----
Asics Gel Lyte iii
An ASICS GEL-Lyte III has emerged dressed in matte hues. Rather than lay flat like a desert horizon, the Blue Mirage and sandy tan pop out from their tonal variants. The Blue Mirage is accompanied by a heavier blue on the Tiger Stripes, and the tan by a lighter beige around the sock liner and split tongue.
5000->4350.00
8,8.5,9.5,10,11,11.5
resources/topic/asics/gel_lyte_iii/asics_gel_lyte_iii_blue_mirage_1.jpg./.resources/topic/asics/gel_lyte_iii/asics_gel_lyte_iii_blue_mirage_2.jpg./.resources/topic/asics/gel_lyte_iii/asics_gel_lyte_iii_blue_mirage_3.jpg
Asics
-----
Asics Gel Lyte Runner
The slick GEL-Lyte Runner sees a transformation in color with new take on the classic shoe. The new "Chameleoid Mesh" iteration features a color-changing appearance on the mesh upper which gives off a different look every step depending on the viewing angle.
4480
7,7.5,8,8.5,9,9.5,10
resources/topic/asics/gel_lyte_runner_chameleon/asics_gel_lyte_runner_chameleon_1.jpg./.resources/topic/asics/gel_lyte_runner_chameleon/asics_gel_lyte_runner_chameleon_2.jpg./.resources/topic/asics/gel_lyte_runner_chameleon/asics_gel_lyte_runner_chameleon_3.jpg
Asics
-----
Asics Gel Lyte III Oreo Pack
Mocked up in a simple but endearing "Oreo" colorway is the latest rework of its coveted GEL-Lyte III model. The cookie-inspired color scheme is available in two colorways.
6732->6700.00
8,8.5,9,9.5,10,10.5,12
resources/topic/asics/asics_gel_lyte_iii_oreo_pack/asics_gel_lyte_iii_oreo_pack_03.jpg./.resources/topic/asics/asics_gel_lyte_iii_oreo_pack/asics_gel_lyte_iii_oreo_pack_04.jpg./.resources/topic/asics/asics_gel_lyte_iii_oreo_pack/asics_gel_lyte_iii_oreo_pack_05.jpg
Asics
-----
AsicsGel Lyte III Japanese Textile Sneaker
We get a closer look at ASICS ravishing new polka dot-themed GEL-Lyte III, officially dubbed the "Japanese Textile" edition. Paying homage to the company�s longstanding Japanese roots, this iconic GEL-Lyte III model gets treated with high quality Japanese selvedge denim for that extra premium touch.
6700.00
7,8,9.5,10,11,12
resources/topic/asics/asics_gel_lyte_iii_japanese_textile_sneaker/asics_gel_lyte_iii_japanese_textile_sneaker_2.jpg./.resources/topic/asics/asics_gel_lyte_iii_japanese_textile_sneaker/asics_gel_lyte_iii_japanese_textile_sneaker_6.jpg./.resources/topic/asics/asics_gel_lyte_iii_japanese_textile_sneaker/asics_gel_lyte_iii_japanese_textile_sneaker_7.jpg
Asics
-----
Ronnie Fieg Kith Asics Gel Lyte III 3-1 Super Green
A couple of weeks ago, Ronnie Fieg announced that he�d be bringing back the elusive "Super Green" edition of the GEL-Lyte III in honor of KITH�s fifth anniversary. Originally released back in 2013, the kicks were created alongside footwear-centric charity Soles4Souls and limited to just 300 pairs that were given away to those less fortunate in Haiti. Now Fieg has officially unveiled the release, as well as the first-ever GEL-Lyte 3.1 that he promised alongside it.
8800.00
8,8.5,9.5,10,10.5,11,11.5
resources/topic/asics/ronnie_fieg_kith_asics_gel_lyte_iii_3_1_super_green/ronnie_fieg_kith_asics_gel_lyte_iii_3_1_super_green_1.jpg./.resources/topic/asics/ronnie_fieg_kith_asics_gel_lyte_iii_3_1_super_green/ronnie_fieg_kith_asics_gel_lyte_iii_3_1_super_green_4.jpg./.resources/topic/asics/ronnie_fieg_kith_asics_gel_lyte_iii_3_1_super_green/ronnie_fieg_kith_asics_gel_lyte_iii_3_1_super_green_5.jpg
Asics
-----
Atmos Asics Gel Lyte V Black Gold
Back in August of 2011, Japanese premium sneaker boutique atmos released a beautiful Black/Gold colorway for its collaborative GEL-Lyte III. This 15th anniversary year, the shop is working with ASICS once again, transferring the clean and semi-subtle colorway onto the sock-like GEL-Lyte V. A black overall silhouette covers the shoe while hits of gold line the lateral and medial ASICS striping and font throughout. A speckled midsole adds a bit of flair while the black leather, suede and mesh materials combine sportiness with luxury. Look for these to pre-sale at atmos on December 5 with a general release later on December 19 for approximately $140 USD.
6160.00
7,8,9.5,10,11,12
resources/topic/asics/atmos_asics_gel_lyte_v_black_gold/atmos_asics_gel_lyte_v_black_gold_1.jpg./.resources/topic/asics/atmos_asics_gel_lyte_v_black_gold/atmos_asics_gel_lyte_v_black_gold_2.jpg./.resources/topic/asics/atmos_asics_gel_lyte_v_black_gold/atmos_asics_gel_lyte_v_black_gold_4.jpg
Asics
-----
BAIT Helps ASICS Celebrate 25 Years of the GEL-Lyte III
ASICS has tapped BAIT for a pristine celebration. Commemorating the split tongue�s 25-year benchmark, the GEL-Lyte III has been chosen as the canvas. Lavish materials such as reptile and tilapia skin cover the shoe�s upper along with hexagonal perforations and reflective silver paneling. Additionally, the mix of sport and luxury is what makes the upcoming runner stand out among the rest. This special edition GEL-Lyte II will release exclusively at BAIT stores via in-store raffle on September 26. An online raffle will be held also.
7000.00
7,7.5,9.5,10,11,12
resources/topic/asics/bait_asics_gel_lyte_iii_25th_anniversary/bait_asics_gel_lyte_iii_25th_anniversary_1.jpg./.resources/topic/asics/bait_asics_gel_lyte_iii_25th_anniversary/bait_asics_gel_lyte_iii_25th_anniversary_2.jpg./.resources/topic/asics/bait_asics_gel_lyte_iii_25th_anniversary/bait_asics_gel_lyte_iii_25th_anniversary_3.jpg
Asics
-----
Air Footscape Chukka
Built with a primarily woven upper and leather accents on the eyestay and heel tab, the Nike Air Footscape Desert Chukka combines a bold look with timeless comfort in a mid-cut profile. The design�s lightweight woven construction is further complimented with the natural motion Footscape tooling to ensure flexable comfort while hustling through busy city-streets.
8610.39->8500.00
7,7.5,9.5,10,11,12
resources/topic/nike/Air_Footscape_Chukka/Air_Footscape_Chukka(1).jpg./.resources/topic/nike/Air_Footscape_Chukka/Air_Footscape_Chukka(2).jpg./.resources/topic/nike/Air_Footscape_Chukka/Air_Footscape_Chukka(3).jpg
Nike
-----
The Nike Air Huarache Ultra Black White
The Nike Air Huarache Ultra Men Shoe features a perforated foam and mesh upper and midsole-outsole combo for enhanced breathability and lightweight comfort. An elastic strap and rubber cage on the heel delivers a classic Huarache look.
5740.26->4539.59
7,7.5,9.5,10,11,12
resources/topic/nike/huarache/nike_air_huarache_run_ultra-black_white_1_1.jpg./.resources/topic/nike/huarache/nike_air_huarache_run_ultra-black_white_1_2.jpg./.resources/topic/nike/huarache/W-AIR-HUARACHE-RUN-ULTRA-819151_001_A_PREM.jpg
Nike
-----
NIKE ROSHE TWO FLYKNIT
Simplicity redefined, the Nike Roshe Two Flyknit Shoe features breathable Nike Flyknit material and three layers of cushioning for soft comfort.
6212.11
7,7.5,9.5,10,11,12
resources/topic/nike/Nike_Roshi_Two_Lyknit/Nike_Roshi_Two_Lyknit1.jpg./.resources/topic/nike/Nike_Roshi_Two_Lyknit/Nike_Roshi_Two_Lyknit2.jpg./.resources/topic/nike/Nike_Roshi_Two_Lyknit/Nike_Roshi_Two_Lyknit3.jpg
Nike
-----
Nike Air Force 1
Shoe updates a hoops original with grid top-stitching for unforgettable style and a classic, court-inspired silhouette.
5017.90
7,7.5,9,10,11,12
resources/topic/nike/Nike_Air_Force_1_07_Mid_LV8/Nike_Air_Force_1_07_Mid_LV8(1).jpg./.resources/topic/nike/Nike_Air_Force_1_07_Mid_LV8/Nike_Air_Force_1_07_Mid_LV8(2)./.resources/topic/nike/Nike_Air_Force_1_07_Mid_LV8/Nike_Air_Force_1_07_Mid_LV8(3).jpg
Nike
-----
Shadow Original Black
Padded collar and tongue delivers a comfy fit. Comfortable fabric lining and a cushioned EVA sockliner for all-day comfort. Traditional lace up front provides optimum fit. "Marshmallowy" EVA midsole cushions and absorbs shock. TPU heel piece provides extra support. Classic Saucony Originals triangular rubber tread pattern.
3380.00
7,7.5,8,9,9.5,10,10.5,11
resources/topic/saucony/Shadow_Original_Black/2108_518_1_1200x735.jpg./.resources/topic/saucony/Shadow_Original_Black/2108_518_2_1200x735.jpg./.resources/topic/saucony/Shadow_Original_Black/2108_518_4_1200x735.jpg
Saucony
-----
Jazz Original Charcoal White
Lace-up front with a padded collar and tongue Nylon/suede upper for long wear Padded tongue, collar, and footbed provides superior shock absorption, fit, and extra comfort.
2420
7,8,9,9.5,10,10.5,11,12
resources/topic/saucony/Jazz_Original_Charcoal_White/2044_274_1_1200x735.jpg./.resources/topic/saucony/Jazz_Original_Charcoal_White/2044_274_2_1200x735.jpg./.resources/topic/saucony/Jazz_Original_Charcoal_White/2044_274_4_1200x735.jpg
Saucony
-----
Running Lancer
Heel PROGRID and medial support arc provide premium cushioning and a touch of support. New midfoot weld provides lightweight lockdown.
3520.00->2639.56
7,7.5,8,9,9.5,10,10.5,11
resources/topic/saucony/Running_Lancer_2/S25264_2_1.jpg./.resources/topic/saucony/Running_Lancer_2/S25264_2_2.jpg./.resources/topic/saucony/Running_Lancer_2/S25264_2_4.jpg
Saucony
-----
Grid Life on the Run
Almost three years in the making, the GRID SD running silhouette and technology marked the first time that a system simultaneously absorbed shock and provide stability for runners during their training. Originally introduced in 1991, this silhouette became an excellent performance trainer for all types of runners.
3960
7,8,9,9.5,10,10.5,11,12
resources/topic/saucony/Grid_Life_On_The Run/S70274_1_1.jpg./.resources/topic/saucony/Grid_Life_On_The Run/S70274_1_2.jpg./.resources/topic/saucony/Grid_Life_On_The Run/S70274_1_4.jpg
Saucony
-----
Bullet Black
Nylon upper with suede overlays is comfortable and stylish, the perfect combination Low profile silhouette and cross-country platform creates an authentic retro feel.
2420
7,7.5,8,9,9.5,10,10.5,11
resources/topic/saucony/Bullet/2943_6_1_1200x735.jpg./.resources/topic/saucony/Bullet/2943_6_2_1200x735.jpg./.resources/topic/saucony/Bullet/2943_6_4_1200x735.jpg
Saucony
-----
la-mjc diadora all gone n9000
To help commemorate the one year anniversary milestone of its flagship store, Club 75 is set to unveil five new sneaker releases during this season�s Paris Fashion Week. Paying homage to the book �All Gone," a street-wise narrative on the best streetwear finds of the previous 12-months, we see here the first drop from the La MJC and Diadora alliance, called the �All Gone" Diadora N9000. Drawing direct design cues from the fourth release published in 2009; the new silhouette gets a clean white upper, constructed from a premium soft leather/mesh combo, with lively red accents lining the base, sole, and laces. 
8845.84->7371.53
7,7.5,8,9,9.5,10,10.5,11
resources/topic/diadora/la_mjc_all_gone_n9000/la-mjc-all-gone-diadora-n9000-1.jpeg./.resources/topic/diadora/la_mjc_all_gone_n9000/la-mjc-all-gone-diadora-n9000-4.jpg./.resources/topic/diadora/la_mjc_all_gone_n9000/la-mjc-all-gone-diadora-n9000-5.jpg
diadora
-----
Dashhex TR Training Shoes 
Showcase your full potential with the DashHex TR from Reebok. The upper is constructed with PU-coated overlays, while the midsole flaunts 3D Ultralite EVA foam to provide improved cushioning and stability. - PU coated leather overlays- Low-cut design- 3D Ultralite EVA foam midsole for lightweight cushioning and stability- Midsole cradle for stabilizing medial and lateral movements- Low-profile, one-piece mid- to outsole with rubber for traction- Forefoot flex grooves- Memory Tech sockliner.
3195.00
6,7,7.5,8,8.5
resources/topic/reebok/Dashhex_TR_Training_Shoes/reebok_5215_819246_2.jpg./.resources/topic/reebok/Dashhex_TR_Training_Shoes/reebok_5220_819246_4.jpg./.resources/topic/reebok/Dashhex_TR_Training_Shoes/reebok_5224_819246_6.jpg
Reebok
-----
Hayasu Training Shoes
Cultivate your own dance daring drive in these Hayasu from Reebok. It has been engineered to support your foot through a full range of movement with its 3D Ultralite cushions feet from impacts. It features Specialised TurnZone and grooves on the sole keep feet light while maximizing metatarsal flexibility to keep you dancing. - Seamless neoprene bootie construction - Mid-cut shape for extra ankle support and improved stability- 3D Ultralite midsole foam - Medial and lateral soft thermoplastic polyurethane pieces for midfoot support- Minimal rubber sole- Moulded Ortholite sockliner.
5295.00
6,6.7,7,7.5,8,8.5,9
resources/topic/reebok/Hayasu_Training_Shoes/reebok_5198_919246_2.jpg./.resources/topic/rebook/Hayasu_Training_Shoes/reebok_5204_919246_4.jpg./.resources/topic/reebok/Hayasu_Training_Shoes/reebok_5209_919246_6.jpg
Reebok
-----
Hexaffect Sport Running Shoes
Your marathon performance will improve if you have a dependable footwear like the Hexaffect Sport Running Shoes from Reebok. Designed to improve brathability and cushioning, Reebok equipped this pair with a lightweight mesh upper and hexagon-shaped flex nodes on the midsole.- Lightweight mesh upper with a midfoot cage- Low-cut design- Hexagon shaped flex nodes on the midsole for natural foot flexibility- Unique mesh collar at the heel for added comfort- Rubber pods on the outsole- 3-D effect on the side for visual dimension- Best for long distance running, Ragnar Relay Races.
3495.00
7,8,9,9.5,10,11,12
resources/topic/reebok/Hexaffect_Sport_Running_Shoes/reebok_6786_886246_2.jpg./.resources/topic/reebok/Hexaffect_Sport_Running_Shoes/reebok_6789_886246_4.jpg./.resources/topic/reebok/Hexaffect_Sport_Running_Shoes/reebok_6792_886246_6.jpg
Reebok
-----
Sublite XT Cushion MT Running Shoes
The Sublite XT Running Shoes from Reebok will easily keep up with your pace. The pair is constructed with sandwich mesh upper to give you maximum breathability and support.- Sandwich mesh upper- Low-cut design for easy ankle mobility and quick stride transitions- New foam compound technology- Full-foot flex grooves- Rubber compound added to the outsole in the heel and forefoot for traction- 3D FuseFrame toecap and vamp for a seamless and lightweight running detail- Upper materials and graphics.
3795.00
9,9.5,10,10.5,11,12
resources/topic/reebok/Sublite_XT_Cushion_MT_Running_Shoes/reebok_8908_076745_2.jpg./.resources/topic/reebok/Sublite_XT_Cushion_MT_Running_Shoes/reebok_8907_076745_1.jpg./.resources/topic/reebok/Sublite_XT_Cushion_MT_Running_Shoes/reebok_8910_076745_3.jpg
Reebok
-----
TrailGrip RS 5.0 Outdoor Shoes
Walk every terrain with the Trailgrip RS 4.0 shoes from Reebok. The upper is constructed with mesh and high-abrasion synthetic details to protect your feet, while the sole is designed with rubber active traction lug to give you better support and grip.- Mesh and high-abrasion synthetic details for breathable, protective durability- Low-cut design for freedom of motion at ankle and quicker stride transitions- DMX Ride tech transfers air within the foam outsole for super shock absorption- Microweb lacing and custom tongue loop for a locked-in fit and trail-friendly style- Rubber active traction lug outsole for gritty grip from street to summit- EVA midsole with stability bar in forefoot for confident steadiness in all terrains.
3995.00
8,8.5,9,9.5,10
resources/topic/reebok/TrailGrip_RS_5.0_Outdoor_Shoes/reebok_2398_748526_2.jpg./.resources/topic/reebok/TrailGrip_RS_5.0_Outdoor_Shoes/reebok_2397_748526_1.jpg./.resources/topic/reebok/TrailGrip_RS_5.0_Outdoor_Shoes/reebok_240_748526_6.jpg
Reebok
-----
Adidas NMD XR1
Flaunting its defining primeknit construction in a mixture of blue and grey tones, more tonal blue hits adorn the signature TPU lacing cage apparatus across its side panels, alongside respective branding on the tongue and rear. In addition, pops of red are utilized for its heel pull tabs, while a full-length boost sole unit in white finishes off the profile.
11704.00
8,8.5,9,9.5,10
resources/topic/adidas/nmd_xr1/nmd_xr1_1.PNG./.resources/topic/adidas/nmd_xr1/nmd_xr1_2.PNG./.resources/topic/adidas/nmd_xr1/nmd_xr1_3.PNG
Adidas
-----
Shadow 6000 Premium Aquamarine
Electric teal, rich purple and vivid cerulean blue draw inspiration from the coloring used to dye eggs, it is a classic Easter intensified - Lace-up front with a padded collar and tongue - Suede upper for long wear
4344.80
7,7.5,8,9,9.5,10,10.5,11
resources/topic/saucony/Shadow_6000_aquamarine/Shadow_6000_blue_1.jpg./.resources/topic/saucony/Shadow_6000_aquamarine/Shadow_6000_blue_2.jpg./.resources/topic/saucony/Shadow_6000_aquamarine/Shadow_6000_blue_3.jpg
Saucony