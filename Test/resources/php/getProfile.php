<?php

if(isset($_FILES['files'])){    
    $file = $_FILES['files'];
    session_start();

    //File properties
    $file_name = $file['name'];
    $file_tmp = $file['tmp_name'];
    $file_size = $file['size'];
    $file_error = $file['error'];

    $file_ext = explode('.',$file_name);
    $file_ext = strtolower(end($file_ext));

    $allowed = array('txt','jpg','png');

    if(in_array($file_ext,$allowed)){
        if($file_error===0){            

            $file_destination = '../images/user/'.$file_name;
            if(move_uploaded_file($file_tmp,$file_destination)){                                                            
                $img_src = "resources/images/user/".$file_name;
                $_SESSION['profile_src']=$img_src;                
                echo $img_src;
            }else{
                echo "error 2 ";
            }                
        }else{
            print_r($file);
        }
    }
    echo "<script>window.history.back()</script>";
}

?>