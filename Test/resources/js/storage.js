function setReferenceNum(){
    var curr_ref = localStorage.getItem('reference');
    if(curr_ref==null){
        curr_ref = 0;
    }else{
        curr_ref++;
    }
    localStorage.setItem('reference',curr_ref);
    return curr_ref;
}

function setItem(id,size){

    var arrCart = JSON.parse(sessionStorage.getItem('cart'));
    if(arrCart==null){
        arrCart = [];
    }
    var ref_no_curr = setReferenceNum();
    var product = {prod_id: id, prod_size: size, ref_no:ref_no_curr};
    arrCart.push(product);
    sessionStorage.setItem('cart',JSON.stringify(arrCart));
    console.log('stored' +id);
}

function getItem(id){
    var item = localStorage.getItem('checkout');
    document.getElementById(id).textContent = item;
}

function getAllItem(){
    var arrCart = JSON.parse(sessionStorage.getItem('cart'));
    var stringSet;
    var i = 0;
    var price = 0;
    arrCart.forEach(function(prod_set){
        product = getProd(prod_set.prod_id);
        i++;
        loadCart(product,prod_set);

        price = price + parseFloat(product.prod_price);
        /*

        console.log(prod_price);
        if(price==0){
            price = parseFloat(prod_price);

        }else{
            price = price + parseFloat(prod_price);
        } */
    });
    console.log(price);
    document.getElementById("summary_quantity").textContent = i;
    document.getElementById("summary_price").textContent = "Php. "+price;
    //document.getElementById().textContent = stringSet;
}


function loadCart(product,prod_set){
    var main_div = document.createElement("div");
    main_div.setAttribute("id","prod_item_div");
    var img = document.createElement("img");
    img.setAttribute("id","prod_image");
    var img_src = product.prod_img_src.split("./.");
    img.src = img_src[0];
    //desc_div----------------------------
    var desc_div = document.createElement("div");
    desc_div.setAttribute("id","prod_desc");
    var h1 = document.createElement("h1");
    h1.setAttribute("id","prod_name");
    h1.textContent = product.prod_name;
    var h2_size = document.createElement("h2");
    h2_size.setAttribute("id","prod_size");
    h2_size.textContent = "Size: "+prod_set.prod_size;
    var h2_price = document.createElement("h2");
    h2_price.setAttribute("id","prod_price");
    h2_price.textContent = "Php. "+product.prod_price;

    desc_div.appendChild(h1);
    desc_div.appendChild(h2_size);
    desc_div.appendChild(h2_price);
    //desc_div----------------------------
    var a = document.createElement("a");
    a.addEventListener("click",function(event){
        removeItem(prod_set);
    });
    a.setAttribute("href","cart.html");
    a.setAttribute("target","_parent");
    var img_a = document.createElement("img");
    img_a.src = "resources/images/ic_clear_white_48dp_1x.png";
    a.appendChild(img_a);

    main_div.appendChild(img);
    main_div.appendChild(desc_div);
    main_div.appendChild(a);

    document.getElementById("prod_con_div").appendChild(main_div);
}

function removeItem(prod_set){
    var val = confirm('are you sure to remove this from your cart?');
    if(val==true){
    var arrCart = JSON.parse(sessionStorage.getItem('cart'));
    if(arrCart==null){
        arrCart = [];
    }
    var index = 0;
    arrCart.forEach(function(prod_elem){
        if(prod_set.ref_no==prod_elem.ref_no){
            arrCart.splice(index,1);
            sessionStorage.setItem('cart',JSON.stringify(arrCart));
            console.log('removed' +prod_elem.ref_no);
            return 0;
        }
        index++;
    });
    }

}




/*
var reader = new XMLHttpRequest() || new ActiveXObject('MSXML2.XMLHTTP');

function loadFile() {
    reader.open('get','../test.txt', true);
    reader.onreadystatechange = displayContents;
    reader.send(null);
}

function displayContents() {
    if(reader.readyState==4) {
        var el = document.getElementById('prod_name');
        el.textContent = reader.responseText;
        //el.innerHTML = reader.responseText;
    }
}*/

function FileHelper()
{}
{
    FileHelper.readStringFromFileAtPath = function(pathOfFileToReadFrom)
    {
        var request = new XMLHttpRequest();
        request.open("GET", pathOfFileToReadFrom, false);
        request.send(null);
        var returnValue = request.responseText;
        return returnValue;
    }
}

function view(img){
    console.log(img);
    var prod_img = document.getElementById('prod_image');
    prod_img.src = img;
}

function loadFile(){
    console.log('loaded item');
    prod_id = localStorage.getItem('open_item');

    product = getProd(prod_id);
    console.log(product);
    brand = getSelectBrandbyName(product.brand_name);
    console.log(brand);
    document.getElementById('prod_brand').src = brand.brand_logo_src;          
    document.getElementById('prod_brand').addEventListener('click',function(Event){       
        openBrand(brand.brand_id);
        window.open('brand.html','_self');        
    });
    
    document.getElementById('prod_name').textContent = product.prod_name;

    var listElem = document.createElement("li");
    var item = document.createElement("a");
    //item.setAttribute("onclick","view('"+arrSplit[i]+"');");
    img_src = product.prod_img_src;
    img_src = img_src.split("./.");

    var img = document.createElement("img");        img.setAttribute("id","image_list");
    img.src = img_src[0];
    item.appendChild(img);
    item.addEventListener("click",function(event){
        view(img_src[0]);
    });
    listElem.appendChild(item);        document.getElementById("prod_item_list").appendChild(listElem);
    var listElem = document.createElement("li");
    var item = document.createElement("a");
    //item.setAttribute("onclick","view('"+arrSplit[i]+"');");

    var img = document.createElement("img");        img.setAttribute("id","image_list");
    img.src = img_src[1];
    item.appendChild(img);
    item.addEventListener("click",function(event){
        view(img_src[1]);
    });
    listElem.appendChild(item);        document.getElementById("prod_item_list").appendChild(listElem);
    var listElem = document.createElement("li");
    var item = document.createElement("a");
    //item.setAttribute("onclick","view('"+arrSplit[i]+"');");

    var img = document.createElement("img");        img.setAttribute("id","image_list");
    img.src = img_src[2];
    item.appendChild(img);
    item.addEventListener("click",function(event){
        view(img_src[2]);
    });
    listElem.appendChild(item);        document.getElementById("prod_item_list").appendChild(listElem);


    document.getElementById("prod_image").src = img_src[0];
    document.getElementById("prod_desc").textContent = product.prod_desc;
    document.getElementById("prod_stock").textContent = product.prod_stock;

    if(product.prod_price_disc!==null){
        document.getElementById("prod_strike").textContent = "Php. "+product.prod_price_disc;
    }
    document.getElementById("prod_price").textContent = "Php. "+product.prod_price;

    console.log("options: "+product.prod_size);

    var arrSize = product.prod_size.split(",");
    var select  = document.createElement("select");
    select.setAttribute("id","size_select");
    for(i = 0; i<arrSize.length; i++){
        var option = document.createElement("option");
        option.setAttribute("value",arrSize[i]);
        option.textContent = arrSize[i];
        select.appendChild(option);
    }
    $('#prod_desc_div').append(select);
    //------------------------------------------------------- set product item for adding to cart
    var buy = document.createElement("a");
    buy.addEventListener("click",function(event){
        //var size =  $('#select_size').find('option:selected').text();
        var size = $("#size_select>option:selected").text();
		
		var hr = new XMLHttpRequest();
    	// Create some variables we need to send to our PHP file
    	var url = "order.php";
    	var fn = product.prod_id;
    	var ln = product.prod_name;
		var pn = product.prod_price;
    	var vars = "id=" +fn+ "&name=" +ln+ "&price=" + pn;
    	hr.open("POST", url, true);
		
    	hr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    	// Access the onreadystatechange event for the XMLHttpRequest object
    	hr.onreadystatechange = function() {
	    	if(hr.readyState == 4 && hr.status == 200) {
		    	var return_data = hr.responseText;
				document.getElementById("status").innerHTML = return_data;
	    	}
    	}
    	// Send the data to PHP now... and wait for response to update the status div
    	hr.send(vars); // Actually execute the request
    	
		
        setItem(product.prod_id,size);
        alert(product.prod_name+' is added to cart ');
    })
    //-------------------------------------------------------------------------------------------
    buy.textContent = "add to cart";
    $('#prod_desc_div').append(buy);
}
