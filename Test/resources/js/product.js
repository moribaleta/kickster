//-------------------------------file open-----------------------------------
var file;
var boolBrand;
function getFiles(){
    if(getBrand()==null){   
        processBrandFile();
        processProductFiles();

    }
    loadContentMain();
}

function loadContentMain(){
    loadSaleRow();
    loadTop10Row();
    loadBrands();
}

function loadTop10Row(){
    var row_container = document.createElement("div");
    row_container.setAttribute("id","row");
    var h1 = document.createElement("h1");
    h1.textContent = "popular";
    row_container.appendChild(h1);
    getTop10Prod().forEach(function(product){
        var container = document.createElement("div");
        container.setAttribute("id","container");
        //---------------- div info------------------------
        var info = document.createElement("div");
        info.setAttribute("id","info");
        var prod_name = document.createElement("h2");
        prod_name.textContent = product.prod_name;

        var prod_price = document.createElement("price");
        prod_price.textContent = 'Php. '+product.prod_price;
        var a = document.createElement("a");
        a.setAttribute("target","_parent");
        a.setAttribute("href","product_buy.html");
        a.addEventListener("click",function(event){
            openItem(product.prod_id);
        });
        a.textContent = "buy me";
        var br = document.createElement("br");
        info.appendChild(prod_name);
        info.appendChild(br);
        if(product.prod_price_disc!=null){
            var prod_strike = document.createElement("strike");
            prod_strike.textContent = 'Php. '+product.prod_price_disc;
            info.appendChild(prod_strike);
        }

        info.appendChild(prod_price);
        info.appendChild(br);
        info.appendChild(br);
        info.appendChild(a);
        //---------------- div info------------------------
        var img = document.createElement("img");
        var src = product.prod_img_src.split("./.");
        img.setAttribute("id","container_img");        
        img.setAttribute('src',src[0]);

        console.log(img);
        container.appendChild(info);
        container.appendChild(img);

        //-----------------div container--------------------------------        

        /*$(document).ready(function(){
            var shadow;
            var vibrant = new Vibrant(img);
            var swatches = vibrant.swatches();
            for (var swatch in swatches){
                if (swatches.hasOwnProperty(swatch) && swatches[swatch]){
                    console.log(swatch, swatches[swatch].getHex());
                    if(swatch == 'DarkMuted'){                      
                        shadow = swatches[swatch].getHex();                        
                    }
                }
            }
           
            /*container.onmouseover =function() {
                                $(this).css("box-shadow"," 0 15px 10px "+ shadow);
                                $(this).css("transition","all .3s ease-in-out");
                            };
            $(container).hover(
                function() {
                                $(this).css("box-shadow"," 0 10px 10px .2px"+ shadow);
                                $(this).css("transition","all .3s ease-in-out");
                            },
                            function(){
                                $(this).css("box-shadow", 'none');
                                $(this).css("transition","all .3s ease-in-out");
                            }
            );
        });*/
        /*for (var swatch in swatches){
            if (swatches.hasOwnProperty(swatch) && swatches[swatch]){
                console.log(swatch, swatches[swatch].getHex());
                if(swatch == 'Vibrant'){                      
                    shadow = swatches[swatch].getHex();
                    $(container).hover(
                        function() {
                            $(this).css("box-shadow"," 0 15px 10px "+ shadow);
                            $(this).css("transition","all .3s ease-in-out");
                        },
                        function(){
                            $(this).css("box-shadow","none");
                            $(this).css("transition","all .3s ease-in-out");
                        }
                    );
                }
            }
        }
*/

        row_container.appendChild(container);

    });

    document.getElementById("content_container").appendChild(row_container);
}



function loadBrands(){

    var row_container = document.createElement("div");
    row_container.setAttribute("id","row");
    var h1 = document.createElement("h1");
    h1.textContent = "brands";
    row_container.appendChild(h1);

    getBrand().forEach(function(brand){
        console.log(brand);
        //-----------------div container--------------------------------
        var container = document.createElement("div");
        container.setAttribute("id","container_brands");
        //---------------- a ------------------------
        var a = document.createElement("a");
        a.setAttribute("target","_parent");
        a.setAttribute("href","brand.html");
        a.addEventListener("click",function(event){
            openBrand(brand.brand_id);
        });
        var img = document.createElement("img");
        img.src = brand.brand_logo_src;
        a.appendChild(img);
        container.appendChild(a);
        row_container.appendChild(container);
    });

    document.getElementById("content_container").appendChild(row_container);
}

function loadSaleRow(){

    var row_container = document.getElementById('row_sale');

    getProduct_Sale().forEach(function(product){
        //-----------------div container--------------------------------
        var container = document.createElement("div");
        container.setAttribute("id","container");
        //---------------- div info------------------------
        var info = document.createElement("div");
        info.setAttribute("id","info");
        var prod_name = document.createElement("h2");
        prod_name.textContent = product.prod_name;
        var prod_strike = document.createElement("strike");
        prod_strike.textContent = 'Php. '+product.prod_price_disc;
        var prod_price = document.createElement("price");
        prod_price.textContent = 'Php. '+product.prod_price;
        var a = document.createElement("a");
        a.setAttribute("target","_parent");
        a.setAttribute("href","product_buy.html");
        a.addEventListener("click",function(event){
            openItem(product.prod_id);
        });
        a.textContent = "buy me";
        var br = document.createElement("br");
        info.appendChild(prod_name);
        info.appendChild(prod_strike);
        info.appendChild(br);
        info.appendChild(prod_price);
        info.appendChild(br);
        info.appendChild(br);
        info.appendChild(a);
        //---------------- div info------------------------
        var img = document.createElement("img");
        var src = product.prod_img_src.split("./.");
        img.src = src[0];

        container.appendChild(info);
        container.appendChild(img);
        //-----------------div container--------------------------------
        row_container.appendChild(container);
    });

}




//-------------------------------------------file op---------------------------------------------------
/*function FileHelper()
{}
{
    FileHelper.readStringFromFileAtPath = function(pathOfFileToReadFrom)
    {
        var request = new XMLHttpRequest();
        request.open("GET", pathOfFileToReadFrom, true);
        request.send(null);
        var returnValue = request.responseText;
        return returnValue;
    }
}*/
function readTextFile(file)
{
    var rawFile = new XMLHttpRequest();
    var file_return;
    rawFile.open("GET", file, false);
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        {
            if(rawFile.status === 200 || rawFile.status == 0)
            {
                file_return = rawFile.responseText;
            }
        }
    }
    rawFile.send(null);
    return file_return;
}

//------------------------------------------------------------------------------------------------------


//-----------------------------------------product op---------------------------------------------------
function storeProducts(prod_list){
    sessionStorage.setItem('prod_list',JSON.stringify(prod_list));
}

function getProductBrand(brand_name){
    var prod_list = JSON.parse(sessionStorage.getItem('prod_list'));
    var prod_list_brand = new Array();
    prod_list.forEach(function(item){
        if(item.brand_name == brand_name)
            prod_list_brand.push(item);
    });
    return prod_list_brand;
    //document.getElementById().textContent = stringSet;
}

function getProduct_Sale(){
    var prod_list = JSON.parse(sessionStorage.getItem('prod_list'));
    var prod_list_sale = new Array();
    prod_list.forEach(function(item){
        if(item.prod_price_disc!=null)
            prod_list_sale.push(item);
        /*if(item.prod_price.indexOf('->')!==-1)
            prod_list_sale.push(item);*/
    });
    return prod_list_sale;
}

function getProd(id){
    var prod_list = JSON.parse(sessionStorage.getItem('prod_list'));
    var prod_list_sale = new Array();
    var return_val;
    prod_list.some(function(item){
        if(id==item.prod_id){
            console.log("found this bitch "+item.prod_name);
            return_val = item;
        }
    });
    /*prod_list.forEach(function(item){
        if(id==item.prod_id){
            console.log("found this bitch "+item.prod_name);
            return_val = item;
            break;
        }
        /*if(item.prod_price.indexOf('->')!==-1)
            prod_list_sale.push(item);
    });*/
    return return_val;
}

function getTop10Prod(){
    var prod_list = JSON.parse(sessionStorage.getItem('prod_list'));
    var prod_list_sale = new Array();
    var return_val;
    var count = 0;

    prod_list.some(function(item){
        if(count<10){
            prod_list_sale.push(item);
        }
        count++;
    });
    return prod_list_sale;
}

function getAllProd(){
    var prod = JSON.parse(sessionStorage.getItem('prod_list'));
    return prod;
}



function product_attr(){
    this.prod_id;
    this.prod_name;
    this.prod_desc;
    this.prod_stock;
    this.prod_price_disc;
    this.prod_price;
    this.prod_size;
    this.prod_img_src;
    this.brand_name;
}


function processProductFiles(){

    var text = readTextFile('resources/topic/product.txt')
    var dt_product = text.split("-----");
    var arr_product = [];
    var i = 0;
    dt_product.forEach(function(product){
        product = product.substr(2);
        passData(product);
        arr_product.push(process_item(i,product.split("\n")));
        i++;
    });
    arr_product.pop;
    storeProducts(arr_product);
}

function process_item(id,product_item){
    var product = new product_attr();
    product.prod_id= id;
    product.prod_name= product_item[0];
    product.prod_desc= product_item[1];
    product.prod_stock= product_item[2];
    var price = product_item[3].split("->");
    if(price.length>1){
        product.prod_price_disc= price[0];
        product.prod_price= price[1];
    }else{
        product.prod_price_disc= null;
        product.prod_price= product_item[3];
    }
    product.prod_size= product_item[4];
    product.prod_img_src= product_item[5];
    product.brand_name= product_item[6];
    return product;
}

//-------------------------------------------------------------------------------------------------------------


//-------------------------brand op----------------------------------------------------------------------------
function brand_attr(brand_id,brand_name,brand_logo_src,brand_logo_w_src,brand_img_src){
    this.brand_id = brand_id;
    this.brand_name = brand_name;
    this.brand_logo_src = brand_logo_src;
    this.brand_logo_w_src = brand_logo_w_src;
    this.brand_img_src = brand_img_src;
}

function storeBrand(brand_list){
    sessionStorage.setItem('brand_list',JSON.stringify(brand_list));
}

function getBrand(){
    var brand = JSON.parse(sessionStorage.getItem('brand_list'));
    return brand;
}

function getSelectBrand(id){
    var brand = JSON.parse(sessionStorage.getItem('brand_list'));
    var return_val;
    brand.forEach(function(item){
        if(id==item.brand_id)
            return_val = item;
    });
    return return_val;
}

function getSelectBrandbyName(name){
    var brand = JSON.parse(sessionStorage.getItem('brand_list'));
    var return_val;
    brand.forEach(function(item){
        if(name==item.brand_name)
            return_val = item;
    });
    return return_val;
}


function processBrandFile(text){

    //var xml = FileHelper.readStringFromFileAtPath ('resources/topic/brand.txt');
    var text = readTextFile('resources/topic/brand.txt');
    var dt_brand = text.split("-----");
    var arr_brand = new Array();
    var i = 0;

    dt_brand.forEach(function(brand_item){
        brand_item = brand_item.substr(2);
        brand_item = brand_item.split("\n");
        var brand = new brand_attr(
            i,
            brand_item[0],
            brand_item[1],
            brand_item[2],
            brand_item[3]
        );
        i++;
        arr_brand.push(brand);
        //console.log(brand.brand_id+" "+brand.brand_name);
    });
    storeBrand(arr_brand);
}
//--------------------------------------------------------------------------------------------------------------
