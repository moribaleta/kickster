function FileHelper()
{}
{
    FileHelper.readStringFromFileAtPath = function(pathOfFileToReadFrom)
    {
        var request = new XMLHttpRequest();
        request.open("GET", pathOfFileToReadFrom, false);
        request.send(null);
        var returnValue = request.responseText;
        return returnValue;
    }
}


function loadFile_Brands(){
    var brand_id = localStorage.getItem('brand');
    //var text = FileHelper.readStringFromFileAtPath(path);
    brand = getSelectBrand(brand_id);

    loadHead(brand);

    getProductBrand(brand.brand_name).forEach(function(product){
        loadContent(product);
    });
    /*for(var i = 1; i<arrSplit.length; i++){
        loadContent(arrSplit[i]);
    }*/

}

function loadHead(brand){
    console.log(brand.brand_id);
    document.title = "Kickster "+brand.brand_name;
    document.getElementById("brand_logo").src = brand.brand_logo_w_src;
    console.log(brand.brand_name);
    // document.getElementById("brand_wallpaper").src = brand.brand_img_src;
    var img = document.getElementById('brand_wallpaper');
    img.setAttribute('src',  brand.brand_img_src);

    img.addEventListener('load', function() {
        var vibrant = new Vibrant(img);
        var swatches = vibrant.swatches()
        for (var swatch in swatches){
            if (swatches.hasOwnProperty(swatch) && swatches[swatch]){
                console.log(swatch, swatches[swatch].getHex())
                if(swatch=='Vibrant'){
                    $('#background').css("background",swatches[swatch].getHex());
                }
            }
        }
        /*
     * Results into:
     * Vibrant #7a4426
     * Muted #7b9eae
     * DarkVibrant #348945
     * DarkMuted #141414
     * LightVibrant #f3ccb4
     */
    });

}

function loadContent(product){
    console.log("item "+ product.name);

    var container = document.createElement("div");
    container.setAttribute("id","container");
    var info = document.createElement("div");
    info.setAttribute("id","info");
    var prod_name = document.createElement("h2");
    prod_name.textContent = product.prod_name;
    info.appendChild(prod_name);

    var prod_price = document.createElement("price");
    if(product.prod_price.indexOf("->")!==-1){
        var value = product.prod_price.split("->");
        var strikePrice = document.createElement("strike");
        strikePrice.textContent ="Php. "+value[0];
        prod_price.textContent = "Php. "+value[1];
        info.appendChild(strikePrice);
    }else{
        prod_price.textContent = "Php. "+product.prod_price;
    }

    info.appendChild(prod_price);
    var a = document.createElement("a");
    a.textContent = "buy me";
    a.setAttribute("href","product_buy.html");
    a.addEventListener("click",function(event){
        openItem(product.prod_id);
    });
    var img = document.createElement("img");
    img.setAttribute("id","prod_image");
    var img_src = product.prod_img_src.split("./.");
    img.src = img_src[0];

    info.appendChild(a);

    container.appendChild(info);
    container.appendChild(img);
    document.getElementById("row").appendChild(container);
}
