function isInView(elem){
   return $(elem).offset().top - $(window).scrollTop() < $(elem).height() ;
}
