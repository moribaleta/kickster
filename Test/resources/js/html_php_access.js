function openBrand(brand_id){
    /*console.log('open'+brand_id);
    var brand = document.getElementById("form_brand");
    temp_storage = document.getElementById('temp_storage_brand');
    document.getElementById('form_brand').submit();*/
    window.open('brand_page.php?temp_storage_brand='+brand_id,'_parent');
}

function openItem(prod_id){
    window.open('product_page.php?temp_storage_item='+prod_id,'_parent');
}

function openDialog(user,prod_id){
    if(user){
        savetocart(prod_id);
    }else{
        modal.style.opacity = 1;            
        modal.style.display = "block";
    }
}

function savetocart(prod_id){        
    console.log(prod_id);
    var r = confirm("add this product to cart?");
    if(r){
        var prod_price = document.getElementById('prod_price').textContent;
        var select = document.getElementById('size_select');
        var prod_size = select.options[select.selectedIndex].value;
        var prod_img = document.getElementById('prod_img').src;
        var prod_name = document.getElementById('prod_name').textContent;
        prod_price = prod_price.substr(4,prod_price.length);        
        setItemCart(prod_id,prod_size,prod_price,prod_img,prod_name);       
        alert("product added to cart");
        //window.open('resources/php/cart_session.php?prod_id='+prod_id+'&prod_size='+prod_size+'&type='+1,'_parent');
    }        

}


function setItemCart(id,size,price,image,name){

    var arrCart = JSON.parse(sessionStorage.getItem('cart'));
    if(arrCart==null){
        arrCart = [];
    }    
    var product = {prod_id: id, prod_name:name, prod_size: size, prod_price:price,prod_img:image};
    arrCart.push(product);
    sessionStorage.setItem('cart',JSON.stringify(arrCart));
    console.log('stored' +id);
}

function getAllItemCart(){
    var arrCart = JSON.parse(sessionStorage.getItem('cart'));
    var stringSet;
    var i = 0;
    var price = 0;
    arrCart.forEach(function(prod_set){       
        i++;
        loadtoCart(prod_set);        
        price = price + parseFloat(prod_set.prod_price);
        /*
        console.log(prod_price);
        if(price==0){
            price = parseFloat(prod_price);

        }else{
            price = price + parseFloat(prod_price);
        } */
    });
    console.log(price);
    document.getElementById("summary_quantity").textContent = i;
    document.getElementById("summary_price").textContent = "Php. "+price;
    //document.getElementById().textContent = stringSet;
}

function sendCart(){
    var r = confirm('confirm purchase?');
    if(r){        
        var arrId = Array(), arrSize = Array();
        var arrCartId = JSON.parse(sessionStorage.getItem('cart'));
        arrCartId.forEach(function(elem){
            arrId.push(elem.prod_id);
            arrSize.push(elem.prod_size);
        });
        var jsonId = JSON.stringify(arrId);
        var jsonSize = JSON.stringify(arrSize);
        var price = document.getElementById("summary_price").textContent;
        price = price.substr(4,price.length);
        window.open("resources/php/loadout.php?jsonId="+jsonId+"&jsonSize="+jsonSize+"&price="+price,'_parent');
    }
    /*
    $.ajax({
       type:"POST",
        url:"resources/php/loadout.php",
        data:{prod_arr_id: jsonId,prod_arr_size:jsonSize},
        cache:false,
        success: function(){
            console.log("succes");
        }
    });
    */

}

function loadtoCart(prod_set){
    var main_div = document.createElement("div");
    main_div.setAttribute("id","prod_item_div");
    var img = document.createElement("img");
    img.setAttribute("id","prod_image");   
    img.src = prod_set.prod_img;
    //desc_div----------------------------
    var desc_div = document.createElement("div");
    desc_div.setAttribute("id","prod_desc");
    var h1 = document.createElement("h1");
    h1.setAttribute("id","prod_name");
    h1.textContent = prod_set.prod_name;
    var h2_size = document.createElement("h2");
    h2_size.setAttribute("id","prod_size");
    h2_size.textContent = "Size: "+prod_set.prod_size;
    var h2_price = document.createElement("h2");
    h2_price.setAttribute("id","prod_price");
    h2_price.textContent = "Php. "+prod_set.prod_price;

    desc_div.appendChild(h1);
    desc_div.appendChild(h2_size);
    desc_div.appendChild(h2_price);
    //desc_div----------------------------
    var a = document.createElement("a");
    a.addEventListener("click",function(event){
        removeItemCart(prod_set);
    });
    a.setAttribute("href","cart.html");
    a.setAttribute("target","_parent");
    var img_a = document.createElement("img");
    img_a.src = "resources/images/ic_clear_white_48dp_1x.png";
    a.appendChild(img_a);

    main_div.appendChild(img);
    main_div.appendChild(desc_div);
    main_div.appendChild(a);

    document.getElementById("prod_con_div").appendChild(main_div);
}


function removeItemCart(product){
    var val = confirm('are you sure to remove this from your cart?');
    if(val==true){
        var arrCart = JSON.parse(sessionStorage.getItem('cart'));
        if(arrCart==null){
            arrCart = [];
        }
        var index = 0;
        arrCart.forEach(function(prod_elem){
            if(product.prod_id == prod_elem.prod_id && product.prod_size==prod_elem.prod_size &&product.prod_img == prod_elem.prod_img){
                arrCart.splice(index,1);
                sessionStorage.setItem('cart',JSON.stringify(arrCart));
                console.log('removed' +prod_elem.ref_no);
                window.location.reload();
                return 0;
            }
            index++;
        });
    }

}

function add_form(op){
    window.open(op+'_form.php','_blank');    
}

function update_form(op,id){
    window.open(op+'_form.php?id='+id,'_blank');    
}

function remove(id,type){
    window.open('resources/php/delete.php?id='+id+'&type='+type,'_parent'); 
}
