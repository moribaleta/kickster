<?php
echo "<html>

        <head>

            <title>kickster brands</title>
            <link rel='stylesheet' href='resources/css/style_test.css'>
            <link rel='stylesheet' href='resources/css/content.css'>
            <script src='resources/js/jquery-3.0.0.min.js'></script>
            <script src='resources/js/header_scroll.js'></script>
            <script src='resources/js/brands.js'></script>
            <script src='resources/js/storage.js'></script>
            <script src='resources/js/product.js'></script>   
            <script src='resources/js/html_php_access.js'></script>

        </head>

        <body onload='loadBrands();'>
            <div id = 'header' class='header' style='opacity:1;'>
                <div id='header_logo' href='index.html'>
                    <a href='index.html'>
                        <img src='resources/images/bannerlogoblack.png'   >
                    </a>
                    <div id='nav_header'>
                        <a href='index.html'>home</a>
                        <a href='sale.php'>sale</a>
                        <a href='brand.php'>brands</a>
                        <a href='About_us.html'>about us</a>
                        <a href='cart.html'>cart</a>
                        <a href='login.php'>login</a>
                    </div>
                </div>
            </div>

            <div id = 'con' class='con' style='padding-top:100px; overflow-y:auto; height:500px; padding-bottom:500px;'>

                <div id='content_container'>
                ".brands()."

                </div>
            </div>
            <script src='resources/js/storage.js'></script>

        </body>


    </html>";

function brands(){
    require('resources/php/connection.php');
    $sql = "select * from brand_tbl";
    $result = $con->query($sql);
    $catch = "";
    if($result->num_rows>0){
        
        while($row = $result->fetch_assoc()) {

            $img = explode("./.",$row["brand_logo"]);

            $catch .= "<div id = 'container_brands'> 
                    "."<a target='_parent' onclick='openBrand(".$row["brand_id"].")'><img src='".$img[0]."' ></a></div>";                
        }

    }else{
        echo "unable to retrieve data";
    }
    $con->close();
    return $catch;
}

?>