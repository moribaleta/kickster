<?php

class main{
    
    function sale(){
        require('resources/php/connection.php');

        $sql = "select * from product_tbl where prod_price_before > 0 LIMIT 10";
        $result = $con->query($sql);
        if($result->num_rows>0){
            while($row = $result->fetch_assoc()) {

                $img = explode("./.",$row["prod_img_src"]);
                echo "<div id = 'container' class='fade'> <div id='info'>
                "."<h2>".$row["prod_name"]."</h2>
                "."<strike>Php.".$row["prod_price_before"]."</strike><br>
                "."<price>Php.".$row["prod_price"]."</price><br><br>
                "."<a target = '_parent'
                "."onclick='openItem(".$row["prod_id"].")'>
                "."buy me
                "."</a>
                "."</div>
                "."<img src='".$img[0]."' >
                "."</div>";
            }


        }else{
            echo "unable to retrieve data";
        }
        $con->close();
    }

    function popular(){
        require('resources/php/connection.php');
        $sql = "select * from product_tbl where rating >= 9 ORDER BY rating DESC LIMIT 10";
        $result = $con->query($sql);
        if($result->num_rows>0){
            while($row = $result->fetch_assoc()) {

                $img = explode("./.",$row["prod_img_src"]);
                if($row["prod_price_before"]>0){
                    echo "<div id = 'container' class='fade'> <div id='info'>
                "."<h2>".$row["prod_name"]."</h2>
                "."<price>rating:".$row["rating"]."</price><br>
                "."<strike>Php.".$row["prod_price_before"]."</strike><br>
                "."<price>Php.".$row["prod_price"]."</price><br>
                "."<a target = '_parent'
                "."onclick='openItem(".$row["prod_id"].")'>
                "."buy me
                "."</a>
                "."</div>
                "."<img src='".$img[0]."' >
                "."</div>";
                }else{
                    echo "<div id = 'container' class='fade'> <div id='info'>
                "."<h2>".$row["prod_name"]."</h2>                
                "."<price>rating:".$row["rating"]."</price><br>
                "."<price>Php.".$row["prod_price"]."</price><br>
                "."<a target = '_parent'
                "."onclick='openItem(".$row["prod_id"].")'>
                "."buy me
                "."</a>
                "."</div>
                "."<img src='".$img[0]."' >
                "."</div>";
                }

            }


        }else{
            echo "unable to retrieve data";
        }
        $con->close();
    }
    
    function brands(){
        require('resources/php/connection.php');
        $sql = "select * from brand_tbl";
        $result = $con->query($sql);
        if($result->num_rows>0){
            while($row = $result->fetch_assoc()) {

                $img = explode("./.",$row["brand_logo"]);

                echo "<div id = 'container_brands'> 
                    "."<a target='_parent' onclick='openBrand(".$row["brand_id"].")'><img src='".$img[0]."' ></a></div>";                
            }

        }else{
            echo "unable to retrieve data";
        }
        $con->close();
    }
}
$main = new main();
echo "<div id='row' class='sale_row'>
      <h1 id='first_row'>Sale</h1>";
echo $main->sale() ."</div>";
echo "<div id='row'>
      <h1>Popular</h1>";
echo $main->popular()."</div>";
echo "<div id='row'>
      <h1>brands</h1>";
echo $main->brands()."</div>";

?>