<DOCTYPE! HTML>

    <html>

        <head>

            <title>  kickster admin </title>
            <link rel="shortcut icon" href="ic_logo.png" type="image/png">
            <link rel="stylesheet" href="resources/css/checkout.css">
            <link rel="stylesheet" href="resources/css/brand.css">
            <link rel="stylesheet" href="resources/css/cart.css">           
            <script src="resources/js/jquery-3.0.0.min.js"></script>
            <script src="resources/js/Chart.min.js"></script>     
            <style>
                table tr:nth-child(even) {
                    background-color: lightgrey;
                }
                table tr:nth-child(odd) {
                    background-color: grey;
                }
                td{
                    border-color: grey;                    
                }
                th{
                    background: dimgrey;
                    color: white;
                    font-size: 12px;
                    border-style: none;                    
                }
            </style>
            <?php


            echo"<script>function setChart(){
                    var ctx = document.getElementById('sales').getContext('2d');
                    var myPieChart = new Chart(ctx, {
                        type: 'pie',
                        data: { 
                            ";getSalesMonth();
            echo "
                        }
                    });
                    var ct = document.getElementById('avesales').getContext('2d');
                    var myLineChart = new Chart(ct, {
                        type: 'line',
                        data: {";
            getAveSalesMonth();            
            echo "
                        }
                    });
                }                
                </script>";

            ?>
        </head>

        <body onload="setChart()">
            <div id = "header" class="header">
                <div id="header_logo" href="index.html">
                    <a href="index.html">
                        <img src="resources/images/bannerlogoblack.png">
                    </a>
                </div>
            </div>

            <div id = "con" style="margin-top=100px;">
                <div id="admin_con">
                    <form>
                        <fieldset>
                            <a id='button' href="#iframe" onclick="openSection(1);">product</a>                            
                            <a id='button' href="#iframe" onclick="openSection(2);">brand</a>
                            <a id='button' href="#con" onclick="closeSection();">close</a>

                            <?php
                            echo "<div id=data_pair>";
                            getProd_count();
                            getAveProd_rating();
                            echo "</div>";
                            echo "<div id=data_pair>";
                            getBrand_count();                            
                            getSale();
                            echo "</div>";
                            echo "<div id=data_pair>";
                            getAveSalePerTrans();
                            echo "</div>";
                            ?>
                            <canvas id="sales" width="auto" height="100%"></canvas>
                            <canvas id="avesales" width="auto" height="100%"></canvas>

                        </fieldset>
                        <fieldset style="overflow-x:auto;">
                            <h1>Total Sales latest 10</h1>
                            <?php
                            getLatestSale();
                            ?>
                        </fieldset>
                        <fieldset id="iframe_field" style="display:hidden">
                            <iframe id="iframe" src="" width="100%" height="0"></iframe>
                        </fieldset>
                    </form>
                </div>
            </div>            
            <script>
                function openSection(type){
                    var iframe = document.getElementById('iframe');
                    iframe.height = '100%';
                    if(type==1){
                        //    window.open('admin_access.php?op=product','_blank');                        
                        iframe.src = 'admin_access.php?op=product';                                                
                    }else{
                        //  window.open('admin_access.php?op=brand','_blank');
                        iframe.src = 'admin_access.php?op=brand';           
                    }                    
                }

                function closeSection(){
                    var iframe = document.getElementById('iframe');
                    iframe.height = '0';
                }                
            </script>
        </body>
    </html>


    <?php
    function getLatestSale(){
        require('resources/php/connection.php');

        $sql = "select t.trans_id, t.user_id,u.username, t.cart_id, t.trans_payment,c.cart_amount,c.cart_quantity,t.trans_date,t.trans_ip,t.exp_del_date,
                    u.address,u.email FROM transaction_tbl t JOIN user_tbl u ON u.user_id = t.user_id 
                    JOIN cart_tbl c ON c.cart_id = t.cart_id ORDER BY t.trans_date DESC LIMIT 10";
        $result = $con->query($sql);   

        echo "<table style='background:lightgrey'><tr><th>trans id</th><th>user id</th><th>username</th><th>cart id</th>
                        <th>total payment</th><th>total amount</th><th>total quantity</th><th>transaction date</th>
                        <th>user ip address</th><th>expected delivery date</th><th>location</th>
                    </tr>";
        while($row = $result->fetch_assoc()){
            echo "<tr>
                                <td>".$row['trans_id']."</td>
                                <td>".$row['user_id']."</td>
                                <td>".$row['username']."</td>
                                <td>".$row['cart_id']."</td>
                                <td>".$row['trans_payment']."</td>
                                <td>".$row['cart_amount']."</td>
                                <td>".$row['cart_quantity']."</td>
                                <td>".$row['trans_date']."</td>
                                <td>".$row['trans_ip']."</td>
                                <td>".$row['exp_del_date']."</td>
                                <td>".$row['address']."</td>                            
                            </tr>";
        }                    
        echo "</table>";
        $con->close();
    }

    function getProd_count(){
        require('resources/php/connection.php');
        $sql = "select count(prod_id) as prod_count from product_tbl ";
        $result = $con->query($sql);
        $row = $result->fetch_assoc();
        echo "<p>total products: ".$row['prod_count']."</p>";
        $con->close();
    }

    function getAveProd_rating(){
        require('resources/php/connection.php');
        $sql = "select round(avg(rating),2) as ave_rating from product_tbl ";
        $result = $con->query($sql);
        $row = $result->fetch_assoc();
        echo "<p>average rating products: ".$row['ave_rating']."</p>";
        $con->close();
    }    

    function getBrand_count(){
        require('resources/php/connection.php');
        $sql = "select count(brand_id) as brand_count from brand_tbl ";
        $result = $con->query($sql);
        $row = $result->fetch_assoc();
        echo "<p>total brands: ".$row['brand_count']."</p>";
        $con->close();
    }


    function getSale(){
        require('resources/php/connection.php');
        $sql = "select sum(trans_payment) as total_payment from transaction_tbl ";
        $result = $con->query($sql);
        $row = $result->fetch_assoc();
        echo "<p>total sales: ".$row['total_payment']."</p>";
        $con->close();
    }

    function getAveSalePerTrans(){
        require('resources/php/connection.php');
        $sql = "select avg(trans_payment) as total_payment from transaction_tbl ";
        $result = $con->query($sql);
        $row = $result->fetch_assoc();
        echo "<p>average sales: ".$row['total_payment']."</p>";
        $con->close();
    }

    function getSalesMonth(){
        $array = array("JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC");
        $colors = array('#C5CAE9','#FF8A80','#9C27B0','#FF80AB','#BBDEFB','#8BC34A','#B2EBF2','#A5D6A7','#9E9E9E','#000000','#607D8B','#E0E0E0');
        require('resources/php/connection.php');
        $sql = "select month(trans_date)as month, SUM(trans_payment) as total_payment from transaction_tbl group by month(trans_date)";
        $result = $con->query($sql);
        $res = "";
        $month = "";   
        $color = "";
        if($result){

            while($row = $result->fetch_assoc()){
                $res .= $row['total_payment'].",";
                $month .= "'".$array[$row['month']-1]."',";
                $color .= "'".$colors[$row['month']-1]."',";
            }

        }else{
            echo "fail";
        }
        $res[strlen($res)-1] =='';
        $month[strlen($month)-1] =='';
        $color[strlen($color)-1] =='';
        $con->close();
        echo "labels: [$month],
                            datasets: [{                                
                                label: 'total sales',
                                data: [$res] ,
                                backgroundColor: [$color]
                    }]";

    }


    function getAveSalesMonth(){
        $array = array("JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC");
        require('resources/php/connection.php');
        $sql = "select month(trans_date)as month, AVG(trans_payment) as total_payment from transaction_tbl group by month(trans_date)";
        $result = $con->query($sql);
        $res = "";
        $month = "";   
        $color = "";
        if($result){

            while($row = $result->fetch_assoc()){
                $res .= $row['total_payment'].",";
                $month .= "'".$array[$row['month']-1]."',";                            
            }

        }else{
            echo "fail";
        }
        $res[strlen($res)-1] =='';
        $month[strlen($month)-1] =='';                    
        $con->close();
        echo "labels: [$month],
                            datasets: [{                                
                                label: 'total sales',
                                data: [$res] ,
                                backgroundColor: '#E0E0E0'
                    }]";                

    }



    ?>