

<html>

    <head>

        <title> kickster cart </title>
        <link rel="shortcut icon" href="ic_logo.png" type="image/png">
        <link rel="stylesheet" href="resources/css/brand.css">
        <link rel="stylesheet" href="resources/css/cart.css">
        <script src="resources/js/product.js"></script>
        <script type="text/javascript" src="resources/js/storage.js"></script>
        <script src="resources/js/html_php_access.js"></script>
        <script src="resources/js/jquery-3.0.0.min.js"></script>       
    </head>

    <body onload="getAllItem();">

        <!--div id = "header" class="header">
<div id="header_logo" href="index.html">
<a href="index.html">
<img src="resources/images/bannerlogoblack.png"   >
</a>
<div id="nav_header">
<a href="index.html">home</a>
<a href="sale.php">sale</a>
<a href="brand.php">brands</a>
<a href="About_us.html">about us</a>
<a href="cart.php">cart</a>
<a href="login.html">login</a>
</div>
</div>
</div-->

        <div id = "con" >

            <?php
            
            class prod_cart{
                public $prod_id;
                public $prod_size;
                
                public function __construct($id,$size){
                    $this->prod_id = $id;
                    $this->prod_size = $size;
                }                
                
                public function getId(){
                    return $this->prod_id;
                }
                public function getSize(){
                    return $this->prod_size;
                }                
                
            }
            
            require('resources/php/connection.php');
            session_start();            
            $cart = $_SESSION['cart'];         
            $sql = "select * from cart_detail where cart_id ='$cart'";
            $result = $con->query($sql);
            $quantity = 0;
            $storage = array();
            if($result->num_rows>0){
                $quantity = mysqli_num_rows($result);            
                while($row = $result->fetch_assoc()){
                    $cart_detail = new prod_cart($row['prod_id'],$row['size']);                    
                    array_push($storage,$cart_detail);
                }
            }
            
            
            $amount = 0;
            if(count($storage)>0){
                $amount = getTotalAmount($storage);   
            }
            $con->close();

            //echo $cart;
            echo "<div id='prod_con_div'>
                    <div id='prod_item_div' style='height:150px;'>
                        <div id='summary_div' >";

            echo "<div id='summary_item_div'>
                        <h1>Quantity: </h1>
                        <h2 id='summary_quantity'>".$quantity."</h2>
                        </div>
                        <div id='summary_item_div'>
                            <h1>Total Price: </h1>
                            <h2 id='summary_price'>".$amount."</h2>
                        </div>
                        <div id='summary_item_div'>
                            <a href='checkout.html' >proceed checkout</a>
                        </div>                                               
                    </div>
                </div>";            
            foreach($storage as $key => $value){
                   getItem($value->getId(),$value->getSize());
            } 
    
            function getTotalAmount($storage){
                $amount = 0;
                foreach($storage as $item => $value){                    
                    $amount += getAmount($value);                                        
                }                
                return $amount;
            }

            function getAmount($item){
                $prod_id = $item->getId();
                require('resources/php/connection.php');
                $sql = "select prod_price from product_tbl where prod_id = '$prod_id'";
                $result = $con->query($sql);
                $row = $result->fetch_assoc();
                $val = $row['prod_price'];                    
                $con->close();
                return $val;
            }                       
            
                function getItem($prod_id,$prod_size){
                    require('resources/php/connection.php');
                    $sql = "select * from product_tbl where prod_id = '$prod_id'";
                    $result = $con->query($sql);
                    $row = $result->fetch_assoc();
                    $img = explode("./.",$row['prod_img_src']);
                    echo "<div id='prod_item_div'>
                    <img id='prod_image' src='".$img[0]."'>
                    <div id='prod_desc'>
                    <h1 id='prod_name'>".$row['prod_name']."</h1>
                    <h2 id='prod_price'>Php. ".$row['prod_price']."</h2>         
                    <h2 id='prod_size'>size ".$prod_size."</h2>
                    </div>
                    <a onclick='removefromcart(".$prod_id.",".$prod_size.");'>
                    <img src='resources/images/ic_clear_white_48dp_1x.png'>
                    </a>
                    </div>";
                    $con->close();
                }

            ?>
            <!--div id="prod_item_div">
<img id="prod_image" src="resources/topic/adidas/ultraboost/front_up_ultraboost.jpg">
<div id="prod_desc">
<h1 id="prod_name">ultraboost</h1>
<h2 id="prod_price">Php. 5000</h2>         <h2 id="prod_size">size 5</h2>
</div>
<a onclick="remove(ultraboost);">
<img src="resources/images/ic_clear_white_48dp_1x.png">
</a>
</div-->

        </div>
        </div>

    </body>

</html>
