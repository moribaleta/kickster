<?php

echo "
    <html>

        <head>

            <title>  kickster sale </title>
            <link rel='shortcut icon' href='ic_logo.png' type='image/png'>
            <link rel='stylesheet' href='resources/css/content.css'>
            <link rel='stylesheet' href='resources/css/brand.css'>
            <script src='resources/js/jquery-3.0.0.min.js'></script>
            <script src='resources/js/product.js'></script>
            <script src='resources/js/html_php_access.js'></script>
            <script>
                function functionNavLink(varValue ){
                    document.getElementById('main_iframe').src = varValue;
                }
            </script>

        </head>

        <body>

            <div id= 'external'>

                <div id = 'header' class='header'>
                    <div id='header_logo' href='index.html'>
                        <a href='index.html'>
                            <img src='resources/images/bannerlogoblack.png'   >
                        </a>
                        <div id='nav_header'>
                            <a href='index.html'>home</a>
                            <a href='sale.php'>sale</a>
                            <a href='brand.php'>brands</a>
                            <a href='About_us.html'>about us</a>
                            <a href='cart.html'>cart</a>
                            <a href='login.php'>login</a>
                        </div>
                    </div>

                </div>

                <div id = 'con' style='padding-top:100px; min-height:200px;overflow-y:auto; padding-bottom:100px;'>                    
                    <div id='row_sale' style='position:relative;  display:inline-block; margin-bottom:150px;width:100%;'>
                        <br>
                        <h1 style='text-align:center;'>Sale</h1>
                         ".openSale()."
                    </div>


                </div>
            </div>



            </div>


        </body>


    </html>
";

function openSale(){                          
    require('resources/php/connection.php');
    $sql = 'select * from product_tbl where prod_price_before > 0';
    $result = $con->query($sql);
    $catch = "";
    if($result->num_rows>0){
        while($row = $result->fetch_assoc()) {

            $img = explode('./.',$row['prod_img_src']);
            $catch .= "
            <div id = 'container' class='fade'> 
                <div id='info'>
                    <h2>".$row['prod_name']."</h2>
                    <strike>Php.".$row['prod_price_before']."</strike><br>
                    <price>Php.".$row['prod_price']."</price><br><br>
                    <a target = '_parent' onclick='openItem(".$row["prod_id"].")'
                   >buy me</a>
                </div>
                    <img src='".$img[0]."' >
            </div>";
        }

    }else{
        echo 'unable to retrieve data';
    }
    $con->close();
    return $catch;
}
?>